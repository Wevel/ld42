﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KineticTurretControl : Module
{
    public float range;
    public float damage;
    public float hitChance;
    public float fireDelay;
    public Button fireButton;

    public ValueLED[] leftLEDs;
    public ValueLED[] rightLEDs;

    private KineticTurret weapon;
    private bool canFire = false;

    private float t = 0;

    private float time = 0;

    private void Start ()
    {
        for (int i = 0; i < leftLEDs.Length; i++) leftLEDs[i].value = Random.value;
        for (int i = 0; i < rightLEDs.Length; i++) rightLEDs[i].value = Random.value;
    }

    public void PointerDown ()
    {
        canFire = true;
        t = 0;
    }

    public void PointerUp ()
    {
        canFire = false;
    }

    protected override void UpdateModule (float deltaTime)
    {
        if (weapon == null)
        {
            weapon = new KineticTurret (player);
            weapon.damage = damage;
            weapon.hitChance = hitChance;
            weapon.range = range;
            weapon.fireDelay = fireDelay;
        }

        if (canFire)
        {
            weapon.Update (deltaTime);
            if (weapon.target == null) tryDamageAsteroid (deltaTime);
        }

        if (canFire && isTarget ())
        {
            for (int i = 0; i < leftLEDs.Length; i++) leftLEDs[i].value = Mathf.Lerp (leftLEDs[i].value, ((float)(-t + i) / leftLEDs.Length), Time.deltaTime);
            for (int i = 0; i < rightLEDs.Length; i++) rightLEDs[i].value = Mathf.Lerp (rightLEDs[i].value, ((float)(t + i) / rightLEDs.Length), Time.deltaTime);

            t += deltaTime;
            fireButton.interactable = true;
        }
        else
        {
            for (int i = 0; i < leftLEDs.Length; i++) leftLEDs[i].value = Mathf.Lerp (leftLEDs[i].value, 0, Time.deltaTime);
            for (int i = 0; i < rightLEDs.Length; i++) rightLEDs[i].value = Mathf.Lerp (rightLEDs[i].value, 0, Time.deltaTime);
            fireButton.interactable = false;
        }

        if (isTarget () && !canFire)
        {

            if (time < 0)
            {
                Sound.PlayClip ("Warning1");
                for (int i = 0; i < leftLEDs.Length; i += 2) leftLEDs[i].value = 0;
                for (int i = 0; i < rightLEDs.Length; i += 2) rightLEDs[i].value = 0;
                time += 0.5f;
            }
            else
            {
                for (int i = 0; i < leftLEDs.Length; i += 2) leftLEDs[i].value = 0.3f;
                for (int i = 0; i < rightLEDs.Length; i += 2) rightLEDs[i].value = 0.3f;
                time -= deltaTime;
            }
        }
    }

    protected override IEnumerator Startup ()
    {
        state = ModuleState.Starting;
        //enabledLED.flash = true;
        float t = 0;

        while (t < 3.5f)
        {
            for (int i = 0; i < leftLEDs.Length; i++) leftLEDs[i].value = Mathf.Lerp (leftLEDs[i].value, Random.value, Time.deltaTime);
            for (int i = 0; i < rightLEDs.Length; i++) rightLEDs[i].value = Mathf.Lerp (rightLEDs[i].value, Random.value, Time.deltaTime);
            t += Time.deltaTime;
            yield return null;
        }

        state = ModuleState.Running;
    }

    protected override IEnumerator ShutDown ()
    {
        state = ModuleState.Stopped;
        yield break;
    }

    public bool isTarget ()
    {
        Rect areaRect = new Rect (player.position.x - weapon.range, player.position.y - weapon.range, weapon.range * 2.0f, weapon.range * 2.0f);
        foreach (Asteroid item in player.game.GetAsteroids (areaRect))
        {
            if ((item.position - player.position).sqrMagnitude < weapon.range * weapon.range) return true;
        }
        return player.GetClosestTargetInRange (weapon.range) != null;
    }

    private void tryDamageAsteroid (float deltaTime)
    {
        float bestDist = float.PositiveInfinity;
        float tmpDist;
        Asteroid best = null;

        Rect areaRect = new Rect (player.position.x - weapon.range, player.position.y - weapon.range, weapon.range * 2.0f, weapon.range * 2.0f);
        foreach (Asteroid item in player.game.GetAsteroids (areaRect))
        {
            tmpDist = (item.position - player.position).sqrMagnitude;
            if (tmpDist < weapon.range * weapon.range)
            {
                if (best == null || tmpDist < bestDist)
                {
                    best = item;
                    bestDist = tmpDist;
                }
            }
        }

        if (best != null) best.DoDamage (weapon.damage * deltaTime);
    }
}