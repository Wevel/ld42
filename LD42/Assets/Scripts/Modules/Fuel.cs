﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fuel : Module
{
    public Image fuelBarImage;

    private void Start ()
    {
        if (fuelBarImage.material != null) fuelBarImage.material = new Material (fuelBarImage.material);
    }

    private void OnDestroy ()
    {
        if (fuelBarImage.material != null) Destroy (fuelBarImage.material);
    }

    protected override void UpdateModule (float deltaTime)
    {
        updateBar ();
    }

    protected override IEnumerator Startup ()
    {
        state = ModuleState.Starting;

        float p = 0;
        while (p < 3.0f)
        {
            p += Time.deltaTime;
            setBar ((Mathf.Sin (p * 6.2f) * 0.3f) + 0.35f);

            yield return null;
        }

        updateBar ();

        state = ModuleState.Running;
    }

    protected override IEnumerator ShutDown ()
    {
        state = ModuleState.Stopped;
        yield break;
    }

    private void updateBar ()
    {
        fuelBarImage.material.SetFloat ("_Percent", Mathf.Lerp (fuelBarImage.material.GetFloat ("_Percent"), player.FuelPercent, Time.deltaTime * 5));
    }

    private void setBar (float percent)
    {
        fuelBarImage.material.SetFloat ("_Percent", Mathf.Lerp (fuelBarImage.material.GetFloat ("_Percent"), percent, Time.deltaTime * 5));
    }
}
