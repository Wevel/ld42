﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bolt : MonoBehaviour
{
    public enum State
    {
        Out = 0,
        ScrewingIn = 1,
        In = 2,
        ScrewingOut = 3,
    }

    private const float screwRotation = 360.0f;
    private const float screwTime = 1.0f;

    public Image boltImage;

    public State state { get; private set; }
    private Module module;

    private RectTransform boltTransform;

    public bool DoneUp
    {
        get
        {
            return state == State.In || state == State.ScrewingOut;
        }
    }

    private void Awake ()
    {
        state = State.Out;
        boltTransform = boltImage.GetComponent<RectTransform> ();
        boltImage.gameObject.SetActive (false);
    }

    public void Init (Module module)
    {
        this.module = module;
        state = State.Out;
    }

    public void SetState (State state)
    {
        StopAllCoroutines ();
        this.state = state;

        if (state == State.In)
        {
            boltImage.gameObject.SetActive (true);
            boltTransform.localScale = Vector3.one;
            boltImage.color = Color.white;
            Vector3 rotation = boltTransform.eulerAngles;
            rotation.z = screwRotation;
            boltImage.transform.eulerAngles = rotation;
        }
        else if (state == State.Out)
        {
            Vector3 rotation = boltTransform.eulerAngles;
            rotation.z = 0;
            boltTransform.eulerAngles = rotation;
            boltImage.gameObject.SetActive (false);
        }
    }

    public void BoltClicked ()
    {
        StopAllCoroutines ();

        if (state == State.In || state == State.ScrewingIn) StartCoroutine (screwOut ());
        else if (module.placed) StartCoroutine (screwIn ());

        Sound.PlayClip ("Drill");
    }

    private IEnumerator screwIn ()
    {
        state = State.ScrewingIn;
        boltImage.gameObject.SetActive (true);
        boltTransform.localScale = Vector3.one;
        boltImage.color = Color.white;

       Vector3 rotation = boltTransform.eulerAngles;

        yield return null;

        while (rotation.z > -screwRotation)
        {
            rotation.z -= screwRotation / screwTime * Time.deltaTime;
            boltTransform.eulerAngles = rotation;
            yield return null;
        }

        rotation.z = screwRotation;
        boltImage.transform.eulerAngles = rotation;

        state = State.In;
        module.BoltStateChanged ();
    }

    private IEnumerator screwOut ()
    {
        state = State.ScrewingOut;
        Vector3 rotation = boltTransform.eulerAngles;

        yield return null; 

        while (rotation.z < screwRotation)
        {
            rotation.z += screwRotation / screwTime * Time.deltaTime;
            boltTransform.eulerAngles = rotation;
            yield return null;
        }

        rotation.z = 0;
        boltTransform.eulerAngles = rotation;

        state = State.Out;
        module.BoltStateChanged ();

        yield return new WaitForSeconds (0.2f);

        float scale = 1.0f;
        Color color = Color.white;

        while (scale < 1.5f)
        {
            scale += 1.0f * Time.deltaTime;
            boltTransform.localScale = Vector3.one * scale;

            color.a -= 1.0f * Time.deltaTime;
            boltImage.color = color;
            yield return null;
        }

        while (color.a > 0)
        {
            color.a -= 1.0f * Time.deltaTime;
            boltImage.color = color;
            yield return null;
        }

        boltImage.gameObject.SetActive (false);
    }
}
