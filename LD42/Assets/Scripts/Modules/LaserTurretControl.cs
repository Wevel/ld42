﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LaserTurretControl : Module
{
    public float chargeRate = 0.075f;
    public float dischargeRate = 0.2f;
    public float range;
    public float damage;
    public float hitChance;
    public Button fireButton;
    public ValueLED[] leds;

    private LaserTurret weapon;
    private float value;
    private bool firing = false;
    private bool canFire = false;

    private float time;

    protected override void UpdateModule (float deltaTime)
    {
        if (weapon == null)
        {
            weapon = new LaserTurret (player);
            weapon.damage = damage;
            weapon.hitChance = hitChance;
            weapon.range = range;
        }

        if (firing)
        {
            canFire = false;
            if (value > 0)
            {
                value -= dischargeRate * deltaTime;
                weapon.Update (deltaTime);

                if (weapon.target == null) tryDamageAsteroid (deltaTime);
            }
            else
            {
                value = 0;
                firing = false;
            }

            fireButton.interactable = false;
        }
        else
        {
            if (value < 1)
            {
                value += chargeRate * deltaTime;
            }
            else
            {
                canFire = true;
            }
        }

        fireButton.interactable = canFire && isTarget ();

        for (int i = 0; i < leds.Length; i++)
        {
            if (canFire) leds[i].value = 1;
            else leds[i].value = value - (i / (float)leds.Length * 0.5f);
        }

        if (isTarget () && canFire)
        {

            if (time < 0)
            {
                leds[leds.Length - 1].value = 0;
                time += 0.5f;
                Sound.PlayClip ("Warning1");
            }
            else
            {
                leds[leds.Length - 1].value = 0.3f;
                time -= deltaTime;
            }
        }
    }

    public void Fire ()
    {
        firing = true;
    }

    protected override IEnumerator Startup ()
    {
        state = ModuleState.Starting;
        //enabledLED.flash = true;
        yield return new WaitForSeconds (0.25f);

        state = ModuleState.Running;
    }

    protected override IEnumerator ShutDown ()
    {
        state = ModuleState.Stopped;
        yield break;
    }

    public bool isTarget ()
    {
        Rect areaRect = new Rect (player.position.x - weapon.range, player.position.y - weapon.range, weapon.range * 2.0f, weapon.range * 2.0f);
        foreach (Asteroid item in player.game.GetAsteroids (areaRect))
        {
            if ((item.position - player.position).sqrMagnitude < weapon.range * weapon.range) return true;
        }
        return player.GetClosestTargetInRange (weapon.range) != null;
    }

    private void tryDamageAsteroid (float deltaTime)
    {
        float bestDist = float.PositiveInfinity;
        float tmpDist;
        Asteroid best = null;

        Rect areaRect = new Rect (player.position.x - weapon.range, player.position.y - weapon.range, weapon.range * 2.0f, weapon.range * 2.0f);
        foreach (Asteroid item in player.game.GetAsteroids (areaRect))
        {
            tmpDist = (item.position - player.position).sqrMagnitude;
            if (tmpDist < weapon.range * weapon.range)
            {
                if (best == null || tmpDist < bestDist)
                {
                    best = item;
                    bestDist = tmpDist;
                }
            }
        }

        if (best != null) best.DoDamage (weapon.damage * deltaTime);
    }
}