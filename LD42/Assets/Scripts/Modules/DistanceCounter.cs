﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DistanceCounter : Module
{
    public Image[] numberImages;
    public float turnRate = 0.4f;
    public float distanceMultiplier = 1;
    
    private Vector2 lastPosition;
    float value = 0;

    private void Start ()
    {
        for (int i = 0; i < numberImages.Length; i++)
        {
           if (numberImages[i].material != null) numberImages[i].material = new Material (numberImages[i].material);
        }

        lastPosition = player.position;
    }

    private void OnDestroy ()
    {
        for (int i = 0; i < numberImages.Length; i++)
        {
            if (numberImages[i].material != null) Destroy (numberImages[i].material);
        }       
    }

    protected override void UpdateModule (float deltaTime)
    {
        float distance = Vector2.Distance (lastPosition, player.position);
        value += distance;
        goToNumber (Mathf.FloorToInt (value * distanceMultiplier));
        lastPosition = player.position;
    }

    protected override IEnumerator Startup ()
    {
        state = ModuleState.Starting;

        float[] rates = new float[numberImages.Length];
        for (int i = 0; i < numberImages.Length; i++) rates[i] = (Random.value * 2f) + 0.5f;

        float t = 0;
        while (t < 1.5f)
        {
            t += Time.deltaTime;
            for (int i = 0; i < numberImages.Length; i++)
            {
                numberImages[i].material.SetFloat ("_Angle", numberImages[i].material.GetFloat ("_Angle") - (Time.deltaTime * turnRate * rates[i]));
            }

            yield return null;
        }

        while (!goToNumber (0)) yield return null;

        state = ModuleState.Running;
    }

    protected override IEnumerator ShutDown ()
    {
        state = ModuleState.Stopped;
        yield break;
    }

    public void Reset ()
    {
        lastPosition = player.position;
        value = 0;
    }

    private bool goToNumber (int number)
    {
        bool allDone = true;
        float target, current;
        for (int i = 0; i < numberImages.Length; i++)
        {
            target = -(number % 10) * 0.1f;
            current = numberImages[i].material.GetFloat ("_Angle");

            if (number % 10 == 0) target--;

            if (current - target > Time.deltaTime * turnRate)
            {
                numberImages[i].material.SetFloat ("_Angle", current - (Time.deltaTime * turnRate));
                allDone = false;
            }
            else numberImages[i].material.SetFloat ("_Angle", target);

            number /= 10;
        }

        return allDone;
    }
}
