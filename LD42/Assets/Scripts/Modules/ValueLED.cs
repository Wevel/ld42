﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ValueLED : MonoBehaviour
{
    public Module module;
    public Image image;
    public Color disabledColour = Color.white;
    public Color offColour = Color.white;
    public Color startingColour = Color.white;
    public Color onColour = Color.white;

    public float flashDelay = 0.5f;
    public float value = 0;

    private float flashTimer;
    public bool canFlash = false;

    private void Update ()
    {
        if (module != null)
        {
            if (module.state != ModuleState.Stopped && module.canRun)
            {
                if (value > 0.5f)
                {
                    flashTimer = 0;
                    image.color = onColour;
                }
                else if (value > 0.25f)
                {
                    if (flashTimer < flashDelay || !canFlash)
                    {
                        image.color = startingColour;
                        flashTimer += Time.deltaTime;
                    }
                    else if (flashTimer < 2.0f * flashDelay)
                    {
                        image.color = disabledColour;
                        flashTimer += Time.deltaTime;
                    }
                    else
                    {
                        flashTimer -= 2.0f * flashDelay;
                    }
                }
                else
                {
                    if (flashTimer < flashDelay * 0.5f || !canFlash)
                    {
                        image.color = offColour;
                        flashTimer += Time.deltaTime;
                    }
                    else if (flashTimer < flashDelay)
                    {
                        image.color = disabledColour;
                        flashTimer += Time.deltaTime;
                    }
                    else
                    {
                        flashTimer -= flashDelay;
                    }
                }
            }
            else
            {
                image.color = disabledColour;
            }
        }
    }
}
