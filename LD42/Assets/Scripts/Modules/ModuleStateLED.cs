﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModuleStateLED : MonoBehaviour
{
    public Module module;
    public Image icon;
    public Color disabledColour = Color.white;
    public Color offColour = Color.white;
    public Color startingColour = Color.white;
    public Color onColour = Color.white;

    public float flashDelay = 0.5f;
    public bool flash;

    private float flashTimer;

    private void Update ()
    {
        if (module != null)
        {
            if (module.canRun)
            {
                if (!flash) flashTimer = 0;

                if (flashTimer < flashDelay)
                {
                    switch (module.state)
                    {
                        case ModuleState.Stopped:
                            icon.color = offColour;
                            break;
                        case ModuleState.Starting:
                            icon.color = startingColour;
                            break;
                        case ModuleState.Running:
                            icon.color = onColour;
                            break;
                        case ModuleState.Stopping:
                            icon.color = startingColour;
                            break;
                        default:
                            break;
                    }

                    flashTimer += Time.deltaTime;
                }
                else if (flashTimer < 2.0f * flashDelay)
                {
                    icon.color = disabledColour;
                    flashTimer += Time.deltaTime;
                }
                else
                {
                    flashTimer -= 2.0f * flashDelay;
                }
            }
            else
            {
                icon.color = disabledColour;
            }
        }
    }
}
