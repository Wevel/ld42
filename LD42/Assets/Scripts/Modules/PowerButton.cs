﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerButton : MonoBehaviour {

    public Module module;
    public Image buttonImage;

    public Sprite moduleDisabled;
    public Sprite moduleOff;
    public Sprite moduleStarting;
    public Sprite moduleOn;

    private void Update ()
    {
        if (module.canRun)
        {
            switch (module.state)
            {
                case ModuleState.Stopped:
                    buttonImage.sprite = moduleOff;
                    break;
                case ModuleState.Starting:
                    buttonImage.sprite = moduleStarting;
                    break;
                case ModuleState.Running:
                    buttonImage.sprite = moduleOn;
                    break;
                case ModuleState.Stopping:
                    buttonImage.sprite = moduleStarting;
                    break;
                default:
                    break;
            }
        }
        else
        {
            buttonImage.sprite = moduleDisabled;
        }
    }

    public void PowerButtonClicked ()
    {
        if (module.canRun)
        {
            Sound.PlayClip ("Radar");
            switch (module.state)
            {
                case ModuleState.Stopped:
                case ModuleState.Stopping:
                    module.StartModule ();
                    break;
                case ModuleState.Starting:
                case ModuleState.Running:
                    module.StopModule ();
                    break;
                default:
                    break;
            }
        }
        else
        {
            Sound.PlayClip ("Invalid");
        }
    }
}
