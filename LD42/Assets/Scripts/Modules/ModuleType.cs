﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ModuleType
{
    public string name;
    public Module prefab;

    [TextArea]
    public string description;
    public uint cost;
    public string upgradeFrom;
    public bool owned;
}
