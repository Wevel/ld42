﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextLED : MonoBehaviour
{
    public Module module;
    public Text text;
    public Color disabledColour = Color.white;
    public Color offColour = Color.white;
    public Color startingColour = Color.white;
    public Color onColour = Color.white;

    public float flashDelay = 0.5f;
    public float value = 0;

    private float flashTimer;

    private void Update ()
    {
        if (module != null)
        {
            if (module.state != ModuleState.Stopped && module.canRun)
            {
                if (value > 0.5f)
                {
                    flashTimer = 0;
                    text.color = onColour;
                }
                else if (value > 0.25f)
                {
                    if (flashTimer < flashDelay)
                    {
                        text.color = startingColour;
                        flashTimer += Time.deltaTime;
                    }
                    else if (flashTimer < 2.0f * flashDelay)
                    {
                        text.color = disabledColour;
                        flashTimer += Time.deltaTime;
                    }
                    else
                    {
                        flashTimer -= 2.0f * flashDelay;
                    }
                }
                else
                {
                    if (flashTimer < flashDelay * 0.5f)
                    {
                        text.color = offColour;
                        flashTimer += Time.deltaTime;
                    }
                    else if (flashTimer < flashDelay)
                    {
                        text.color = disabledColour;
                        flashTimer += Time.deltaTime;
                    }
                    else
                    {
                        flashTimer -= flashDelay;
                    }
                }
            }
            else
            {
                text.color = disabledColour;
            }
        }
    }
}
