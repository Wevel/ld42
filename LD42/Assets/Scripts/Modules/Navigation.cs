﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Navigation : Module
{
    public float leftPos = -75;
    public float rightPos = 75;

    public float leftAngle = -60;
    public float rightAngle = 60;

    public Transform destinationMarker;
    public Transform velocityMarker;

    public Image turnRightImage;
    public Image turnLeftImage;
    public Image barImage;
    public Color buttonPressedColor = new Color (200, 200, 200);

    public float markerMoveSpeed = 100;

    private bool rightButtonDown;
    private bool leftButtonDown;

    private void Start ()
    {
        if (barImage.material != null) barImage.material = new Material (barImage.material);
    }

    private void OnDestroy ()
    {
        if (barImage.material != null) Destroy (barImage.material);
    }

    public void TurnRightButtonDown ()
    {
        if (state == ModuleState.Running && player.fuel > 0) rightButtonDown = true;
        else Sound.PlayClip ("Invalid");
    }

    public void TurnRightButtonUp ()
    {
        rightButtonDown = false;
    }

    public void TurnLeftButtonDown ()
    {
        if (state == ModuleState.Running && player.fuel > 0) leftButtonDown = true;
        else Sound.PlayClip ("Invalid");
    }

    public void TurnLeftButtonUp ()
    {
        leftButtonDown = false;
    }

    protected override void UpdateModule (float deltaTime)
    {
        setPosition (destinationMarker, player.contract.target.position - player.position);
        setPosition (velocityMarker, player.velocity);

        barImage.material.SetFloat ("_Angle", -player.direction / 360f);

        player.turnRight = rightButtonDown;
        player.turnLeft = leftButtonDown;

        turnRightImage.color = player.turnRight ? buttonPressedColor : Color.white;
        turnLeftImage.color = player.turnLeft ? buttonPressedColor : Color.white;
    }

    private void setPosition (Transform marker, Vector2 direction)
    {
        float dirAngle = Mathf.Atan2 (direction.x, direction.y) * Mathf.Rad2Deg;
        if (player.direction > 180) dirAngle += player.direction - 360;
        else dirAngle += player.direction;

        if (dirAngle < -180) dirAngle += 360;
        else if (dirAngle > 180) dirAngle -= 360;

        Vector2 pos = Vector2.zero;
        pos.x = Mathf.Lerp (leftPos, rightPos, Mathf.Clamp01 ((dirAngle - leftAngle) / (rightAngle - leftAngle)));
        marker.localPosition = Vector2.Lerp (marker.localPosition, pos, Time.deltaTime * markerMoveSpeed);
    }

    protected override IEnumerator Startup ()
    {
        state = ModuleState.Starting;
        float t = 0;
        float initial = barImage.material.GetFloat ("_Angle");
        while (t < 1.0f)
        {
            t += Time.deltaTime;
            barImage.material.SetFloat ("_Angle", initial + t);
            yield return null;
        }

        state = ModuleState.Running;
    }

    protected override IEnumerator ShutDown ()
    {
        state = ModuleState.Stopped;
        yield break;
    }
}
