﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShieldLevelLED : MonoBehaviour
{
    public Module module;
    public Image icon;
    public Color disabledColour = Color.white;
    public Color offColour = Color.white;
    public Color startingColour = Color.white;
    public Color onColour = Color.white;

    public float flashDelay = 0.5f;
    public int shieldIndex;

    private float flashTimer;

    private void Update ()
    {
        if (module != null)
        {
            if (module.state == ModuleState.Running)
            {
                float shield = module.player.GetShieldLevel (shieldIndex);

                if (shield > 0.5f)
                {
                    flashTimer = 0;
                    icon.color = onColour;
                }
                else if (shield > 0.25f)
                {
                    if (flashTimer < flashDelay)
                    {
                        icon.color = startingColour;
                        flashTimer += Time.deltaTime;
                    }
                    else if (flashTimer < 2.0f * flashDelay)
                    {
                        icon.color = disabledColour;
                        flashTimer += Time.deltaTime;
                    }
                    else
                    {
                        flashTimer -= 2.0f * flashDelay;
                    }
                }
                else
                {
                    if (flashTimer < flashDelay * 0.5f)
                    {
                        icon.color = offColour;
                        flashTimer += Time.deltaTime;
                    }
                    else if (flashTimer < flashDelay)
                    {
                        icon.color = disabledColour;
                        flashTimer += Time.deltaTime;
                    }
                    else
                    {
                        flashTimer -= flashDelay;
                    }
                }
            }
            else
            {
                icon.color = disabledColour;
            }
        }
    }
}
