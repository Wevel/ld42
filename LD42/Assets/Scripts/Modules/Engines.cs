﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Engines : Module
{
    public List<Image> engineImages = new List<Image> ();
    public TextLED fuelLowLED;

    public Image[] leftBars;
    public Image[] rightBars;
    
    private void Start ()
    {
        for (int i = 0; i < engineImages.Count; i++)
        {
            if (engineImages[i].material != null) engineImages[i].material = new Material (engineImages[i].material);
        }

        for (int i = 0; i < leftBars.Length; i++)
        {
            if (leftBars[i].material != null) leftBars[i].material = new Material (leftBars[i].material);
        }

        for (int i = 0; i < rightBars.Length; i++)
        {
            if (rightBars[i].material != null) rightBars[i].material = new Material (rightBars[i].material);
        }
    }

    private void OnDestroy ()
    {
        for (int i = 0; i < engineImages.Count; i++)
        {
            if (engineImages[i].material != null) Destroy (engineImages[i].material);
        }

        for (int i = 0; i < leftBars.Length; i++)
        {
            if (leftBars[i].material != null) Destroy (leftBars[i].material);
        }

        for (int i = 0; i < rightBars.Length; i++)
        {
            if (rightBars[i].material != null) Destroy (rightBars[i].material);
        }
    }

    public void BoostEngine (int index)
    {
        if (state == ModuleState.Running && player.fuel > 0) player.BoostEngine (index);
        else Sound.PlayClip ("Invalid");
    }

    protected override void UpdateModule (float deltaTime)
    {
        updateEngineBar (0);
        updateEngineBar (1);
        fuelLowLED.value = Mathf.Clamp01 (player.FuelPercent * 4);

        if (player.FuelPercent < 0.2f) Sound.PlayClip ("Warning2");

        float value = engineImages[0].material.GetFloat ("_Percent");

        for (int i = 0; i < leftBars.Length; i++)
        {
            setBarOverTime (leftBars[i], (Mathf.Sin ((value + ((float)i / leftBars.Length)) * Mathf.PI * 2.0f) + 1.0f) * 0.5f);
        }

        value = engineImages[1].material.GetFloat ("_Percent");
        for (int i = 0; i < rightBars.Length; i++)
        {
            setBarOverTime (rightBars[i], (Mathf.Sin ((value + ((float)i / rightBars.Length) + 0.5f) * Mathf.PI * 2.0f) + 1.0f) * 0.5f);
        }
    }

    protected override IEnumerator Startup ()
    {
        state = ModuleState.Starting;
        fuelLowLED.value = 0.4f;

        yield return new WaitForSeconds (0.25f);
        
        float p = 0;

        while (p < 2.0f)
        {
            p += Time.deltaTime;
            setEngineBar (0, (Mathf.Sin ((p * 2.0f) + 0.1f) + 1.0f) * 0.5f);
            setEngineBar (1, (Mathf.Sin ((p * 2.7f) + 2.9f) + 1.0f) * 0.5f);

            for (int i = 0; i < leftBars.Length; i++) setBarOverTime (leftBars[i], (Mathf.Sin ((p * (float)(i + 1)) + 0.1f) + 1.0f) * 0.5f);
            for (int i = 0; i < rightBars.Length; i++) setBarOverTime (rightBars[i], (Mathf.Sin ((p * (float)(i + 1)) + 0.8f) + 1.0f) * 0.5f);

            yield return null;
        }

        updateEngineBar (0);
        updateEngineBar (1);

        state = ModuleState.Running;
    }

    protected override IEnumerator ShutDown ()
    {
        state = ModuleState.Stopped;
        yield break;
    }

    private void updateEngineBar (int index)
    {
        engineImages[index].material.SetFloat ("_Percent", Mathf.Lerp (engineImages[index].material.GetFloat ("_Percent"), player.GetEngineLevel (index), Time.deltaTime * 5));
    }

    private void setEngineBar (int index, float percent)
    {
        engineImages[index].material.SetFloat ("_Percent", Mathf.Lerp (engineImages[index].material.GetFloat ("_Percent"), percent, Time.deltaTime * 5));
    }

    private void setBarOverTime (Image image, float value)
    {
        image.material.SetFloat ("_Percent", Mathf.Lerp (image.material.GetFloat ("_Percent"), value, Time.deltaTime * 5));
    }
}
