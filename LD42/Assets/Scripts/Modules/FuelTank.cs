﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuelTank : Module
{
    public float fuelIncreace = 25;

    protected override void UpdateModule (float deltaTime) { }
    protected override IEnumerator Startup ()
    {
        yield return null;
        state = ModuleState.Running;
    }

    protected override IEnumerator ShutDown ()
    {
        state = ModuleState.Stopped;
        yield break;
    }
}
