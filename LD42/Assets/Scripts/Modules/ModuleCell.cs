﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct ModuleCell
{
    public int x { get; private set; }
    public int y { get; private set; }
    public Module module;

    public ModuleCell (int x, int y)
    {
        this.x = x;
        this.y = y;
        module = null;
    }

    public override string ToString ()
    {
        return "(" + x + "," + y + ")";
    }
}
