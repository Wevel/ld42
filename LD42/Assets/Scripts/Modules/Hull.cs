﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hull : Module
{
    public Image healthBarImage;

    private void Start ()
    {
        if (healthBarImage.material != null) healthBarImage.material = new Material (healthBarImage.material);
    }

    private void OnDestroy ()
    {
        if (healthBarImage.material != null) Destroy (healthBarImage.material);
    }

    protected override void UpdateModule (float deltaTime)
    {
        updateBar ();
    }

    protected override IEnumerator Startup ()
    {
        state = ModuleState.Starting;

        float p = 0;
        while (p < 3.0f)
        {
            p += Time.deltaTime;
            setBar ((Mathf.Sin (p * 4.2f) * 0.3f) + 0.35f);

            yield return null;
        }

        updateBar ();

        state = ModuleState.Running;
    }

    protected override IEnumerator ShutDown ()
    {
        state = ModuleState.Stopped;
        yield break;
    }

    private void updateBar ()
    {
        healthBarImage.material.SetFloat ("_Percent", Mathf.Lerp (healthBarImage.material.GetFloat ("_Percent"), player.HealthPercent, Time.deltaTime * 5));
    }

    private void setBar (float percent)
    {
        healthBarImage.material.SetFloat ("_Percent", Mathf.Lerp (healthBarImage.material.GetFloat ("_Percent"), percent, Time.deltaTime * 5));
    }
}
