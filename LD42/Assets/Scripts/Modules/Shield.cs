﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : Module
{
    protected override void UpdateModule (float deltaTime)
    {

    }

    protected override IEnumerator Startup ()
    {
        state = ModuleState.Starting;
        //enabledLED.flash = true;
        yield return new WaitForSeconds (0.25f);

        state = ModuleState.Running;
    }

    protected override IEnumerator ShutDown ()
    {
        state = ModuleState.Stopped;
        yield break;
    }
}
