﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModuleFrame : MonoBehaviour
{
    public Game game;
    public PartsMenu partsMenu;

    public Vector2Int frameSize;
    public Vector2 frameOffset;
    public float cellSize = 100;
    public float moduleMoveSpeed = 300;
    public Transform moduleSpawnLocation;

    public List<ModuleType> moduleTypes = new List<ModuleType> ();
    private List<Module> modules = new List<Module> ();

    private ModuleCell[,] moduleFrame;

    private void Awake ()
    {
        moduleTypes.RemoveAll (x => x.name == "Shield");
    }

    public void RestartGame (Game game)
    {
        this.game = game;
        moduleFrame = new ModuleCell[frameSize.x, frameSize.y];
        for (int x = 0; x < frameSize.x; x++)
        {
            for (int y = 0; y < frameSize.y; y++)
            {
                moduleFrame[x, y] = new ModuleCell (x, y);
            }
        }

        for (int i = 0; i < moduleTypes.Count; i++)
        {
            if (moduleTypes[i].owned)
                GetModule (moduleTypes[i]);
        }
    }

    public void Restart (Game game)
    {
        for (int i = 0; i < modules.Count; i++) modules[i].Restart ();
    }

    public T FindModule<T> () where T : Module
    {
        return (T)modules.Find (x => x is T);
    }

    public List<T> FindAllModules<T> () where T : Module
    {
        List<Module> m = modules.FindAll (x => x is T);
        List<T> t = new List<T> ();
        for (int i = 0; i < m.Count; i++) t.Add ((T)m[i]);
        return t;
    }

    public void GetModule (ModuleType type)
    {
        Debug.Log ("Getting module: " + type.name);

        if (type.upgradeFrom != "")
        {
            Module old = modules.Find (x => x.type.name == type.upgradeFrom);

            if (old == null)
            {
                Debug.LogError ("Not unlocked module: " + type.upgradeFrom);
                return;
            }

            modules.Remove (old);

            // Make sure to clear up all the things for this module
            partsMenu.Take (old);
            RemoveModule (old);

            Destroy (old.gameObject);
        }

        Module module = Instantiate (type.prefab, moduleSpawnLocation.position, Quaternion.identity, transform);
        module.Init (this, game.player, type);
        modules.Add (module);

        if (module.startEquiped)
        {
            for (int y = frameSize.y - 1; y >= 0; y--)
            {
                for (int x = 0; x < frameSize.x; x++)
                {
                    if (CanPlace (module, x, y))
                    {
                        setCells (x, y, module.size.x, module.size.y, module);
                        module.Place (moduleFrame[x, y]);
                        if (type.upgradeFrom == "") module.ScrewInBolts ();
                        return;
                    }
                }
            }
        }
        else
        {
            partsMenu.Dump (module);
        }
    }

    public void RemoveModule (Module module)
    {
        if (module.placed) setCellsNull (module);
    }

    public bool PlaceModule (Module module, Vector2 position)
    {
        Vector2 cell = (position / cellSize) - frameOffset;
        Vector2Int cellIndex = new Vector2Int (Mathf.RoundToInt (cell.x), Mathf.RoundToInt (cell.y));

        //for (int y = frameSize.y - 1; y >= 0; y--)
        //{
            //for (int x = 0; x < frameSize.x; x++)
            //{
                if (CanPlace (module, cellIndex.x, cellIndex.y))
                {
                    if (module.placed) setCellsNull (module);
                    setCells (cellIndex.x, cellIndex.y, module.size.x, module.size.y, module);
                    module.Place (moduleFrame[cellIndex.x, cellIndex.y]);
                    return true;
                }
            //}
        //}

        return false;
    }

    public Vector2 GetMoveLocatoin (ModuleCell cell)
    {
        return new Vector2 ((cell.x + frameOffset.x) * cellSize, (cell.y + frameOffset.y) * cellSize);
    }

    public bool CanPlace (Module module, int x, int y)
    {
        if (x < 0 || y < 0) return false;

        for (int cy = y; cy < y + module.size.y; cy++)
        {
            if (cy >= frameSize.y) return false;
            for (int cx = x; cx < x + module.size.x; cx++)
            {
                if (cx >= frameSize.x) return false;
                if (moduleFrame[cx, cy].module != null && moduleFrame[cx, cy].module != module) return false;
            }
        }

        return true;
    }

    private void setCells (int x, int y, int width, int height, Module module)
    {
        for (int cy = y; cy < y + height; cy++)
        {
            if (cy < frameSize.y)
            {
                for (int cx = x; cx < x + width; cx++)
                {
                    if (cx < frameSize.x) moduleFrame[cx, cy].module = module;
                }
            }
        }
    }

    private void setCellsNull (Module module)
    {
        for (int y = 0; y < frameSize.y; y++)
        {
            for (int x = 0; x < frameSize.x; x++)
            {
                if (moduleFrame[x, y].module == module) moduleFrame[x, y].module = null;
            }
        }
    }

    private void OnDrawGizmosSelected ()
    {
        if (moduleFrame != null)
        {
            for (int x = 0; x < frameSize.x; x++)
            {
                for (int y = 0; y < frameSize.y; y++)
                {
                    Gizmos.color = moduleFrame[x, y].module != null ? Color.red : Color.green;
                    Gizmos.DrawCube (GetMoveLocatoin (moduleFrame[x, y]) + new Vector2 (50, 50), Vector3.one * cellSize * 0.9f);
                }
            }
        }
    }
}
