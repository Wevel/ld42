﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Armour : Module
{
    public float healthIncreace = 50;

    protected override void UpdateModule (float deltaTime) { }
    protected override IEnumerator Startup ()
    {
        yield return null;
        state = ModuleState.Running;
    }

    protected override IEnumerator ShutDown ()
    {
        state = ModuleState.Stopped;
        yield break;
    }
}
