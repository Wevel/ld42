﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ModuleState
{
    Stopped = 0,
    Starting = 1,
    Running = 2,
    Stopping = 3,
    Moving = 4,
}
