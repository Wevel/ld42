﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public abstract class Module : MonoBehaviour
{
    public List<Bolt> bolts = new List<Bolt> ();
    public Vector2Int size;
    public bool autoStart = false;
    public bool startEquiped = false;

    private Coroutine stateCoroutine;

    public ModuleFrame frame { get; private set; }
    public Player player { get; private set; }
    public ModuleState state { get; protected set; }
    public bool canRun { get; private set; }
    public bool canMove { get; private set; }

    public bool placed { get; private set; }
    public ModuleCell mainCell { get; private set; }

    public ModuleType type { get; private set; }

    private RectTransform rt;

    public bool dragging { get; private set; }
    private Vector2 dragOffset;

    private void Awake ()
    {
        rt = this.GetComponent<RectTransform> ();

        EventTrigger et = this.GetComponent<EventTrigger> ();
        if (et == null) et = gameObject.AddComponent<EventTrigger> ();

        EventTrigger.Entry entry = new EventTrigger.Entry ();
        entry.eventID = EventTriggerType.PointerDown;
        entry.callback.AddListener ((eventData) => { MouseDown (); });
        et.triggers.Add (entry);

        entry = new EventTrigger.Entry ();
        entry.eventID = EventTriggerType.PointerUp;
        entry.callback.AddListener ((eventData) => { MouseUp (); });
        et.triggers.Add (entry);

        for (int i = 0; i < bolts.Count; i++)
        {
            bolts[i].Init (this);
        }
    }

    private void Update ()
    {
        if (player != null && player.contract != null)
        {
            if (dragging)
            {
                if (Input.GetMouseButton (0)) Move ((Vector2)Camera.main.ScreenToWorldPoint (Input.mousePosition) - dragOffset);
                else MouseUp ();
            }
            else
            {
                if (state == ModuleState.Running) UpdateModule (Time.deltaTime);
                else if (autoStart && state == ModuleState.Stopped) StartModule ();
            }
        }
        else
        {
            if (dragging)
            {
                if (placed) Move (frame.GetMoveLocatoin (mainCell));
                else frame.partsMenu.Dump (this);
            }
        }
    }

    public void Init (ModuleFrame frame, Player player, ModuleType type)
    {
        this.frame = frame;
        this.player = player;
        this.type = type;

        for (int i = 0; i < bolts.Count; i++) bolts[i].SetState (Bolt.State.Out);
        BoltStateChanged ();
    }

    public void Restart ()
    {
        StopAllCoroutines ();
        state = ModuleState.Stopped;

        if (placed) Move (frame.GetMoveLocatoin (mainCell));
        else frame.partsMenu.Dump (this);
    }

    public void ScrewInBolts ()
    {
        if (placed)
        {
            for (int i = 0; i < bolts.Count; i++) bolts[i].SetState (Bolt.State.In);
            BoltStateChanged ();
        }
    }

    public void TryPlace (Vector2 target)
    {
        if (frame.PlaceModule (this, target))
        {
            placed = true;
        }
        else
        {
            if (placed) Move (frame.GetMoveLocatoin (mainCell));
            else frame.partsMenu.Dump (this);
        }
    }

    public void Place (ModuleCell cell)
    {
        frame.partsMenu.Take (this);

        placed = true;
        mainCell = cell;
        Move (frame.GetMoveLocatoin (cell));
    }

    public void BoltStateChanged ()
    {
        canRun = true;
        canMove = true;

        for (int i = 0; i < bolts.Count; i++)
        {
            if (bolts[i].DoneUp) canMove = false;
            else canRun = false;
        }

        if (state == ModuleState.Running && !canRun) StopModule ();
    }

    public void MouseDown ()
    {
        if (canMove)
        {
            dragging = true;
            transform.SetParent (frame.transform);
            dragOffset = Camera.main.ScreenToWorldPoint (Input.mousePosition) - transform.position;
            frame.partsMenu.menuDropDown.MoveOut ();
            frame.partsMenu.Take (this);
        }
    }

    public void MouseUp ()
    {
        if (dragging)
        {
            dragging = false;

            if (Camera.main.ScreenToWorldPoint (Input.mousePosition).y > frame.partsMenu.dumpY)
            {
                frame.RemoveModule (this);
                frame.partsMenu.Dump (this);
            }
            else
            {
                TryPlace (transform.position);
            }
        }
    }

    public void StartModule ()
    {
        if (canRun)
        {
            state = ModuleState.Starting;
            if (stateCoroutine != null) StopCoroutine (stateCoroutine);
            stateCoroutine = StartCoroutine (Startup ());
        }
    }

    public void StopModule ()
    {
        state = ModuleState.Stopping;
        if (stateCoroutine != null) StopCoroutine (stateCoroutine);
        stateCoroutine = StartCoroutine (ShutDown ());
    }

    public void Move (Vector2 target)
    {
        state = ModuleState.Moving;
        if (stateCoroutine != null) StopCoroutine (stateCoroutine);
        stateCoroutine = StartCoroutine (move (target));
    }

    private void pickUp (Vector2 target)
    {
        placed = false;
        Move (target);
    }

    private IEnumerator move (Vector2 target)
    {
        while (Vector2.Distance (target, rt.anchoredPosition) > frame.moduleMoveSpeed * Time.deltaTime)
        {
            rt.anchoredPosition = Vector2.Lerp (rt.anchoredPosition, target, frame.moduleMoveSpeed * Time.deltaTime);
            //rt.anchoredPosition = Vector2.zero;
            yield return null;
        }

        rt.anchoredPosition = target;
      //  rt.anchoredPosition = Vector2.zero;
        state = ModuleState.Stopped;
    }

    protected abstract void UpdateModule (float deltaTime);

    protected abstract IEnumerator Startup ();
    protected abstract IEnumerator ShutDown ();
}
