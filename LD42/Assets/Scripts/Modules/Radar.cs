﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Radar : Module
{
    public enum Range
    {
        Short = 0,
        Medium = 1,
        Long = 2,
    }

    public enum DisplayType
    {
        Basic,
        SomeTypes,
        AllTypes,
    }

    const float pi2 = Mathf.PI * 2.0f;

    public Image radarScannerImage;
    public ModuleStateLED enabledLED;
    public Image markerImagePrefab;
    public Sprite dotSprite;
    public Sprite triangleSprite;
    public Sprite crossSprite;
    public Sprite squareSprite;
    public float rotateTime = 1.0f;
    public float radarDisplayRange = 60;
    public float radarRange = 250;
    public Color markerColour = Color.white;
    public Range range;
    public Button shortRangeButton;
    public Button mediumRangeButton;
    public Button longRangeButton;
    public DisplayType displayType = DisplayType.Basic;

    private float RadarRange
    {
        get
        {
            switch (range)
            {
                case global::Radar.Range.Short:
                    return radarRange * 0.5f;
                case global::Radar.Range.Medium:
                    return radarRange;
                case global::Radar.Range.Long:
                    return radarRange * 2.0f;
                default:
                    return radarRange;
            }
        }
    }

    private float RotateTime
    {
        get
        {
            switch (range)
            {
                case global::Radar.Range.Short:
                    return rotateTime * 0.5f;
                case global::Radar.Range.Medium:
                    return rotateTime;
                case global::Radar.Range.Long:
                    return rotateTime * 2.0f;
                default:
                    return radarRange;
            }
        }
    }

    private Dictionary<Image, float> currentMarkers = new Dictionary<Image, float> ();

    private float angle;
    private float lastAngle;

    private void LateUpdate ()
    {
        float deltaTime = Time.deltaTime;

        List<Image> keys = new List<Image> (currentMarkers.Keys);
        float time;
        Color colour;
        for (int i = 0; i < keys.Count; i++)
        {
            time = currentMarkers[keys[i]];
            if (time > 0)
            {
                time -= deltaTime;
                currentMarkers[keys[i]] = time;
                colour = markerColour;
                colour.a = time / RotateTime;
                keys[i].material.color = colour;
            }
            else
            {
                Destroy (keys[i].material);
                Destroy (keys[i].gameObject);

                currentMarkers.Remove (keys[i]);
            }
        }

        if (shortRangeButton != null && mediumRangeButton != null && longRangeButton != null)
        {
            shortRangeButton.interactable = range != Range.Short;
            mediumRangeButton.interactable = range != Range.Medium;
            longRangeButton.interactable = range != Range.Long;
        }
    }

    public void SetRange (Range range)
    {
        this.range = range;
    }

    protected override void UpdateModule (float deltaTime)
    {
        rotateRadar (deltaTime);

        foreach (Ship item in player.game.Ships)
        {
            if (item != player) addMarker (item);
        }

        foreach (Asteroid item in player.game.GetAsteroids (new Rect (player.position.x - radarRange, player.position.y - radarRange, radarRange * 2f, radarRange * 2f)))
        {
            addMarker (item);
        }
    }

    protected override IEnumerator Startup ()
    {
        const float spinUpTime = 4.7f;

        state = ModuleState.Starting;
        enabledLED.flash = true;
        yield return new WaitForSeconds (0.25f);

        angle = radarScannerImage.material.GetFloat ("_Angle");

        float p = 0;

        while (p < spinUpTime)
        {
            p += Time.deltaTime;
            rotateRadar (Time.deltaTime * Mathf.Clamp01 (p / spinUpTime));

            yield return null;
        }

        enabledLED.flash = false;
        state = ModuleState.Running;
    }

    protected override IEnumerator ShutDown ()
    {
        state = ModuleState.Stopped;
        yield break;
    }

    private void addMarker (Asteroid ship)
    {
        //Vector2 delta = ship.transform.position - player.transform.position;
        Vector2 delta = ship.position - player.position;
        float dist = delta.sqrMagnitude;
        float r = RadarRange;

        if (dist < r * r)
        {
            float a = Mathf.Atan2 (delta.x, delta.y) + (player.direction * Mathf.Deg2Rad);
            if (a < 0) a += pi2;
            if (a > pi2) a -= pi2;

            if (lastAngle < 0) a -= pi2;

            if (a > lastAngle && a < angle)
            {
                delta *= radarDisplayRange / r;

                Image marker = Instantiate (markerImagePrefab, radarScannerImage.transform);
                marker.sprite = dotSprite;
                marker.transform.localPosition = Quaternion.Euler (0, 0, -player.direction) * delta;
                marker.material = new Material (marker.material);
                currentMarkers[marker] = rotateTime * 0.75f;

                Sound.PlayClip ("Radar");
            }
        }
    }

    private void addMarker (Ship ship)
    {
        //Vector2 delta = ship.transform.position - player.transform.position;
        Vector2 delta = ship.position - player.position;
        float dist = delta.sqrMagnitude;
        float r = RadarRange;

        if (dist < r * r)
        {
            float a = Mathf.Atan2 (delta.x, delta.y) + (player.direction * Mathf.Deg2Rad);
            if (a < 0) a += pi2;
            if (a > pi2) a -= pi2;

            if (lastAngle < 0) a -= pi2;

            if (a > lastAngle && a < angle)
            {
                delta *= radarDisplayRange / r;

                Image marker = Instantiate (markerImagePrefab, radarScannerImage.transform);
                marker.sprite = getSprite (ship);
                marker.transform.localPosition = Quaternion.Euler(0, 0, -player.direction) * delta;
                marker.material = new Material (marker.material);
                currentMarkers[marker] = rotateTime * 0.75f;
                Sound.PlayClip ("Radar");
            }
        }
    }

    private Sprite getSprite (Ship ship)
    {
        switch (displayType)
        {
            case DisplayType.SomeTypes:
                if (ship is Station) return squareSprite;
                //else if (t == typeof ())
                else return dotSprite;
            case DisplayType.AllTypes:
                if (ship is Pirate) return crossSprite;
                else if (ship is Station) return squareSprite;
                //else if (t == typeof ())
                else return dotSprite;
            case DisplayType.Basic:
            default:
                if (ship is Station)
                {
                    if (player.BeenToStation ((Station)ship)) return squareSprite;
                }

                return dotSprite;
        }
    }

    private void rotateRadar (float deltaTime)
    {
        lastAngle = angle;
        angle += deltaTime / RotateTime * pi2;
        if (angle > pi2)
        {
            lastAngle -= pi2;
            angle -= pi2;
        }

        radarScannerImage.material.SetFloat ("_Angle", angle);
    }

    public void SetShortRange ()
    {
        range = Range.Short;
    }

    public void SetMediumRange ()
    {
        range = Range.Medium;
    }

    public void SetLongRange ()
    {
        range = Range.Long;
    }

}
