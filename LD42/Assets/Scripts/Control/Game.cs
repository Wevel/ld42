﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    private class ContractType
    {
        private int _chance;
        public int chance
        {
            get
            {
                return _chance;
            }
            set
            {
                if (_chance > 0.25f * baseChance) _chance = value;
            }
        }

        public int baseChance;
        public string name;
        public uint basePayment;
        public float pirateChance;

        public ContractType (string name, int chance, uint basePayment, float pirateChance)
        {
            this.name = name;
            this.baseChance = chance;
            this._chance = baseChance;
            this.basePayment = basePayment;
            this.pirateChance = pirateChance;
        }
    }

    public Player playerPrefab;

    public Tutorial tutorial;
    public ModuleFrame modules;
    public StationMenu stationMenu;
    public float fuelCostPerUnit = 0.75f;
    public float repairCostPerUnit = 2.0f;

    public Player player { get; private set; }
    public EnemySpawner enemySpawner { get; private set; }
    public uint money { get; private set; }
    public bool paused { get; private set; }

    public float asteroidUpdateRange = 2500;
    public int asteroidChunkSize = 1000;
    public float perlinScale = 0.001f;

    public int completedContracts = 0;

    public enum DeathType
    {
        NotDead,
        NoHealth,
        NoFuel,
    }

    public DeathType deathType { get; private set; }

    public void Pause ()
    {
        paused = true;
    }

    public void UnPause ()
    {
        if (!stationMenu.inStation) paused = false;
    }

    private List<Ship> spawnedShips = new List<Ship> ();
    private List<Station> spaceStations = new List<Station> ();
    private List<Asteroid> asteroidSpawnPoints = new List<Asteroid> ();
    private List<AsteroidChunk> asteroidChunks = new List<AsteroidChunk> ();

    private Vector2 perlinOffset;
    private Station startingStation;

    private List<ContractType> contractTypes = new List<ContractType> ();

    public bool startedContract = false;

    public IEnumerable<Ship> Ships
    {
        get
        {
            for (int i = 0; i < spawnedShips.Count; i++) yield return spawnedShips[i];
        }
    }

    public IEnumerable<Asteroid> GetAsteroids (Rect area)
    {
        for (int i = 0; i < asteroidChunks.Count; i++)
        {
            if (asteroidChunks[i].Intersects (area))
            {
                for (int a = 0; a < asteroidChunks[i].asteroids.Count; a++)
                {
                    yield return asteroidChunks[i].asteroids[a];
                }
            }
        }
    }

    public uint RepairCost
    {
        get
        {
            if (player == null) return 0;

            if (player.health < player.maxHealth) return (uint)Mathf.CeilToInt (repairCostPerUnit * (player.maxHealth - player.health));
            else return 0;
        }
    }

    public uint RefuelCost
    {
        get
        {
            if (player == null) return 0;

            if (player.fuel < player.maxFuel) return (uint)Mathf.CeilToInt (fuelCostPerUnit * (player.maxFuel - player.fuel));
            else return 0;
        }
    }

    private void Awake ()
    {
        enemySpawner = this.GetComponent<EnemySpawner> ();

        contractTypes.Add (new ContractType ("Water", 40, 700, 0.6f));
        contractTypes.Add (new ContractType ("Ore", 30, 500, 0.3f));
        contractTypes.Add (new ContractType ("Fuel", 25, 600, 0.8f));
        contractTypes.Add (new ContractType ("Animal Specimens", 1, 2500, 0.1f));
        contractTypes.Add (new ContractType ("Rare Metals", 10, 1200, 0.7f));
        contractTypes.Add (new ContractType ("Gems", 8, 1700, 0.6f));
        contractTypes.Add (new ContractType ("Electronics", 14, 900, 0.5f));
        contractTypes.Add (new ContractType ("Laser Parts", 10, 1500, 0.8f));
        contractTypes.Add (new ContractType ("Combat Robots", 6, 2200, 1f));
        contractTypes.Add (new ContractType ("Terraforming Equipment", 12, 1300, 0.3f));
    }

    private void Update ()
    {
        if (!paused && player != null)
        {
            float deltaTime = Time.deltaTime;
            for (int i = 0; i < spawnedShips.Count; i++)
            {
                spawnedShips[i].Update (deltaTime);
            }

            if (player != null)
            {
                Rect updateArea = new Rect (player.position.x - asteroidUpdateRange, player.position.y - asteroidUpdateRange, asteroidUpdateRange * 2, asteroidUpdateRange * 2);

                checkAsteroidChunkSpawns (updateArea);

                for (int i = 0; i < asteroidChunks.Count; i++)
                {
                    if (asteroidChunks[i].Intersects (updateArea)) asteroidChunks[i].Update (deltaTime);
                }
            }
        }
    }

    public void StartContract (Contract contract)
    {
        startedContract = true;
        player.StartContract (contract);
        enemySpawner.ContractStarted ();
        modules.Restart (this);
        paused = false;
        //if (completedContracts == 0)
        //CompleteCurrentContract ();
    }

    public Station GetNPCTarget (NPC npc)
    {
        return getContractTarget (npc.position, 800f);
    }

    public void TryBuy (ModuleType type)
    {
        if (money >= type.cost)
        {
            money -= type.cost;
            type.owned = true;
            modules.GetModule (type);
        }
    }

    public void TryRepair ()
    {
        if (money >= RepairCost)
        {
            money -= RepairCost;
            player.FullHeal ();
        }
    }

    public void TryRefuel ()
    {
        if (money >= RefuelCost)
        {
            money -= RefuelCost;
            player.FullRefuel ();
        }
    }

    public void CompleteCurrentContract ()
    {
        startedContract = false;
        enemySpawner.DespawnAll ();
        money += player.contract.payment;
        money += player.contract.bounty;
        completedContracts++;
        stationMenu.EnterStation (player.contract);
        player.StartContract (null);
        paused = true;
    }

    private void checkAsteroidChunkSpawns (Rect rect)
    {
        int xMin = Mathf.FloorToInt (rect.xMin / asteroidChunkSize);
        int yMin = Mathf.FloorToInt (rect.yMin / asteroidChunkSize);
        int xMax = Mathf.CeilToInt (rect.xMax / asteroidChunkSize);
        int yMax = Mathf.CeilToInt (rect.yMax / asteroidChunkSize);

        for (int x = xMin; x < xMax; x++)
        {
            for (int y = yMin; y < yMax; y++)
            {
                if (asteroidChunks.Find (a => a.x == x && a.y == y) == null)
                {
                    asteroidChunks.Add (new AsteroidChunk (
                        x, 
                        y, 
                        this, 
                        new Rect(x * asteroidChunkSize, y * asteroidChunkSize, asteroidChunkSize, asteroidChunkSize), 
                        new Vector2 (x * asteroidChunkSize / perlinScale, y * asteroidChunkSize / perlinScale) + perlinOffset,
                        perlinScale));
                }
            }
        }
    }

    public void StartGame ()
    {
        deathType = DeathType.NotDead;
        money = 0;
        completedContracts = 0;
        enemySpawner.DespawnAll ();
        spawnedShips.Clear ();
        spaceStations.Clear ();
        asteroidChunks.Clear ();
        asteroidSpawnPoints.Clear ();
        player = new Player (this, Vector2.zero, 0);
        player.FullRefuel ();

        perlinOffset = new Vector2 (Random.Range (0f, 0.25f), Random.Range (0f, 0.25f));
        generateMap ();

        modules.RestartGame (this);

        Station targetStation = getContractTarget (player.position, 800f);

        int asteroidCount = 0;
        Rect area = new Rect (
            Mathf.Min (player.position.x, targetStation.position.x),
            Mathf.Min (player.position.y, targetStation.position.y),
            Mathf.Abs (player.position.x - targetStation.position.x),
            Mathf.Abs (player.position.y - targetStation.position.y));

        foreach (Asteroid item in GetAsteroids (area)) asteroidCount++;

        StartContract (new Contract ()
        {
            goodsName = "Nuclear bomb",
            payment = 400,
            target = targetStation,
            asteroidLevel = asteroidCount,
            pirateLevel = 0
        });

        tutorial.StartTutorial ();
    }

    public Contract GenerateContract ()
    {
        float totalContractChance = 0;
        for (int i = 0; i < contractTypes.Count; i++) totalContractChance += contractTypes[i].chance;

        ContractType type = null;
        float rndValue = Random.value * totalContractChance;

        for (int i = 0; i < contractTypes.Count; i++)
        {
            if (rndValue < contractTypes[i].chance)
            {
                type = contractTypes[i];
                break;
            }
            else
            {
                rndValue -= contractTypes[i].chance;
            }
        }

        if (type == null) type = contractTypes[contractTypes.Count - 1];

        type.chance--;

        Station targetStation = getContractTarget (player.position, 800f);

        int asteroidCount = 0;
        Rect area = new Rect (
            Mathf.Min (player.position.x, targetStation.position.x),
            Mathf.Min (player.position.y, targetStation.position.y),
            Mathf.Abs (player.position.x - targetStation.position.x),
            Mathf.Abs (player.position.y - targetStation.position.y));

        foreach (Asteroid item in GetAsteroids (area)) asteroidCount++;

        int asteroidLevel = Mathf.FloorToInt (asteroidCount * 0.03f);

        int numPossiblePirates = Mathf.Max (1, Mathf.FloorToInt (completedContracts * 0.8f));
        int numPirates = 0;
        for (int i = 0; i < numPossiblePirates; i++)
        {
            if (Random.value < type.pirateChance) numPirates++;
        }

        uint reward = ((uint)asteroidLevel + (2u * (uint)numPirates)) * 50u;
        reward += (uint)Mathf.RoundToInt (type.basePayment * (float)(completedContracts + 2.0f) * 0.1f);

        return new Contract ()
        {
            goodsName = type.name,
            payment = reward,
            target = targetStation,
            asteroidLevel = asteroidLevel,
            pirateLevel = numPirates
        };
    }

    public void ShipDied (Ship ship)
    {
        if (spawnedShips.Contains (ship)) spawnedShips.Remove (ship);

        if (ship.GetType () == typeof (Station) && spaceStations.Contains ((Station)ship)) spaceStations.Remove ((Station)ship);

        if (ship is Player)
        {
            paused = true;

            if (ship.health < 0) deathType = DeathType.NoHealth;
            else deathType = DeathType.NoFuel;

            player = null;
        }
    }

    private void generateMap ()
    {
        spawnSpaceStations ();
        checkAsteroidChunkSpawns (new Rect (player.position.x - asteroidUpdateRange, player.position.y - asteroidUpdateRange, asteroidUpdateRange * 2, asteroidUpdateRange * 2));
    }

    private void spawnSpaceStations ()
    {
        const int generatedSpaceStations = 100;
        const float minSpaceStationDistance = 800;
        const float maxSpaceStationDistance = 1500;
        const int maxSpawnAttempts = 10;

        List<Station> canSpawnFrom = new List<Station> ();

        Station s = new Station (this, Vector2.zero, 0);
        startingStation = s;
        spaceStations.Add (s);
        canSpawnFrom.Add (s);

        int index;

        for (int i = 0; i < generatedSpaceStations; i++)
        {
            if (canSpawnFrom.Count > 0)
            {
                index = Random.Range (0, canSpawnFrom.Count);
                s = addSpaceStation (canSpawnFrom[index], minSpaceStationDistance, maxSpaceStationDistance, maxSpawnAttempts);
                if (s == null) canSpawnFrom.RemoveAt (index);
                else canSpawnFrom.Add (s);
            }
            else
            {
                Debug.Log ("Only spawned " + i + " space stations");
                break;
            }
        }
    }

    public void SpawnShip (Ship s)
    {
        if (!spawnedShips.Contains (s)) spawnedShips.Add (s);
    }

    private Station getContractTarget (Vector2 from, float minRange)
    {
        float r2 = minRange * minRange;
        List<Station> possible = spaceStations.FindAll (x => (x.dist = (from - x.position).sqrMagnitude) > r2 && (x != startingStation || completedContracts > 2));
        possible.Sort ((a, b) => { return a.dist.CompareTo (b.dist); });

        return possible[Random.Range (0, Mathf.Min (possible.Count, 5))];
    }

    public bool CanSpawnAsteroid (Vector2 point, float minRange)
    {
        return spaceStations.Find (x => (point - x.position).sqrMagnitude < 10000) == null;
    }

    private Station addSpaceStation (Station from, float minRange, float maxRange, int attempts)
    {
        Vector2 test;
        for (int i = 0; i < attempts; i++)
        {
            test = from.position + Random.insideUnitCircle.normalized * Random.Range (minRange + 1, maxRange);
            if (canSpawnStation (test, minRange))
            {
                Station s = new Station (this, test, 0);
                new NPC (this, test, 0);
                spaceStations.Add (s);
                return s;
            }
        }

        return null;
    }

    private bool canSpawnStation (Vector2 point, float minRange)
    {
        float r2 = minRange * minRange;
        return spaceStations.Find (x => (point - x.position).sqrMagnitude < r2) == null;
    }

    private void OnDrawGizmosSelected ()
    {
        if (spawnedShips != null)
        {
            for (int i = 0; i < spawnedShips.Count; i++) spawnedShips[i].OnDrawGizmos ();

            //for (int i = 0; i < asteroidChunks.Count; i++)
            //{
            //    for (int a = 0; a < asteroidChunks[i].asteroids.Count; a++)
            //    {
            //        asteroidChunks[i].asteroids[a].OnDrawGizmos ();
            //    }
            //}
        }

        //for (int x = -25; x < 25; x++)
        //{
        //    for (int y = -25; y < 25; y++)
        //    {
        //        Gizmos.color = Color.Lerp (Color.red, Color.blue, Mathf.PerlinNoise (perlinOffset.x + (x * 100 / perlinScale), perlinOffset.y + (y * 100 / perlinScale)));
        //        Gizmos.DrawSphere (new Vector3 (x * 100, y * 100), 50);
        //    }
        //}
    }
}
