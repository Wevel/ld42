﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StationMenu : MonoBehaviour
{
    public Game game;

    public GameObject uiBlockingScreen;

    [Header ("Contract End")]
    public MenuDropDown contractEndMenu;
    public Text paymentAmountText;
    public Text bountyAmountText;
    public Text totalAmountText;
    public Text ownedAmountText;

    [Header ("Shop")]
    public MenuDropDown shopMenu;
    public Button repairButton;
    public Button refuelButton;
    public Text repairButtonText;
    public Text refuelButtonText;
    public ModuleListItem listItemPrefab;
    public Transform listItemsHolder;
    public Text selectedListNameText;
    public Text selectedListDescriptionText;
    public Button buySelectedItemButton;
    public Text buySelectedItemButtonText;
    public GameObject selectedObjectHolder;
    public Text remainingMoneyText;

    private List<ModuleListItem> currentListItems = new List<ModuleListItem> ();
    private Queue<ModuleListItem> oldListItems = new Queue<ModuleListItem> ();

    [Header ("Contract Select")]
    public MenuDropDown contractSelectsMenu;
    public List<ContractOption> contractOptions = new List<ContractOption> ();

    private ModuleType selectedType;
    public bool inStation { get; private set; }

    public bool inShop { get; private set; }

    private void Awake ()
    {
        uiBlockingScreen.SetActive (false);

        contractEndMenu.gameObject.SetActive (true);
        shopMenu.gameObject.SetActive (true);
        contractSelectsMenu.gameObject.SetActive (true);
        inShop = false;
        inStation = false;
    }

    private void Update ()
    {
        if (inShop)
        {
            remainingMoneyText.text = "$" + game.money + "M";
            repairButton.interactable = game.player.health < game.player.maxHealth;
            refuelButton.interactable = game.player.fuel < game.player.maxFuel;
            repairButtonText.text = "Repair ($" + game.RepairCost + "M)";
            refuelButtonText.text = "Refuel ($" + game.RefuelCost + "M)";

            selectedObjectHolder.SetActive (selectedType != null);
            if (selectedType != null)
            {
                selectedListNameText.text = selectedType.name;
                selectedListDescriptionText.text = selectedType.description;
                buySelectedItemButtonText.text = "Buy ($" + selectedType.cost + "M)";
                buySelectedItemButton.interactable = game.money >= selectedType.cost;
            }
        }
    }

    public void EnterStation (Contract contract)
    {
        inStation = true;
        inShop = false;
        contractEndMenu.DropIn ();
        uiBlockingScreen.SetActive (true);

        paymentAmountText.text = "$" + contract.payment + "M";
        bountyAmountText.text = "$" + contract.bounty + "M";
        totalAmountText.text = "$" + (contract.payment + contract.bounty) + "M";
        ownedAmountText.text = "$" + game.money + "M";
    }

    public void ShowShop ()
    {
        shopMenu.DropIn ();
        inShop = true;
        updateModulesList ();
    }

    public void ShowContractMenu ()
    {
        inShop = false;
        contractSelectsMenu.DropIn ();

        for (int i = 0; i < contractOptions.Count; i++) contractOptions[i].SetContract (game.GenerateContract ());
    }

    public void SelectModuleType (ModuleType type)
    {
        selectedType = type;
        updateModulesList ();
    }

    private void updateModulesList ()
    {
        for (int i = 0; i < currentListItems.Count; i++)
        {
            currentListItems[i].gameObject.SetActive (false);
            oldListItems.Enqueue (currentListItems[i]);
        }
        currentListItems.Clear ();

        ModuleListItem item;

        for (int i = 0; i < game.modules.moduleTypes.Count; i++)
        {
            if (!game.modules.moduleTypes[i].owned)
            {
                if (game.modules.moduleTypes[i].upgradeFrom == "" || game.modules.moduleTypes.Find (x => x.name == game.modules.moduleTypes[i].upgradeFrom && x.owned) != null)
                {
                    item = getListItem ();
                    item.gameObject.SetActive (true);
                    item.transform.SetAsLastSibling ();
                    item.SetModule (this, game.modules.moduleTypes[i], game.modules.moduleTypes[i] == selectedType);
                    currentListItems.Add (item);
                }
            }
        }
    }

    public void BuySelectedModule ()
    {
        if (selectedType != null)
        {
            game.TryBuy (selectedType);
            SelectModuleType (null);
        }
    }

    public void SelectContract (Contract contract)
    {
        contractEndMenu.MoveOut ();
        shopMenu.MoveOut ();
        contractSelectsMenu.MoveOut ();

        game.StartContract (contract);
        uiBlockingScreen.SetActive (false);
        inStation = false;
    }

    public void Repair ()
    {
        game.TryRepair ();
    }

    public void Refuel ()
    {
        game.TryRefuel ();
    }

    private ModuleListItem getListItem ()
    {
        if (oldListItems.Count > 0) return oldListItems.Dequeue ();
        return Instantiate (listItemPrefab, listItemsHolder);
    }
}
