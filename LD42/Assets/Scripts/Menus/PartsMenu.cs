﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PartsMenu : MonoBehaviour
{
    public MenuDropDown menuDropDown;
    public float dumpY = 200;
    public int spaceX = 8;
    public int spaceY = 4;

    private List<Module> unusedModules = new List<Module> ();
    private bool[,] taken;

    private void Awake ()
    {
        menuDropDown = this.GetComponent<MenuDropDown> ();
    }

    public void PlaceModules ()
    {
        if (taken == null) taken = new bool[spaceX, spaceY];
        for (int x = 0; x < 8; x++)
        {
            for (int y = 0; y < 4; y++)
            {
                taken[x, y] = false;
            }
        }
        unusedModules.Sort (
            (a, b) =>
            {
                return b.size.y.CompareTo (a.size.y);
            });

        for (int i = 0; i < unusedModules.Count; i++)
        {
            if (!tryPlace (unusedModules[i])) Debug.LogWarning ("Failed to place module - " + unusedModules[i].name);
        }
    }

    public void Take (Module module)
    {
        if (unusedModules.Contains (module)) unusedModules.Remove (module);
        PlaceModules ();
    }


    public void Dump (Module module)
    {
        if (!unusedModules.Contains (module)) unusedModules.Add (module);
        module.transform.SetParent (transform);
        PlaceModules ();
    }

    private bool tryPlace (Module module)
    {
        for (int y = spaceY - 1; y >= 0; y--)
        {
            for (int x = 0; x < spaceX; x++)
            {
                if (canPlace (module, x, y))
                {
                    module.Move (new Vector2 ((x * 100) - 400, (y * 100) - 200));
                    for (int cy = y; cy < y + module.size.y; cy++)
                    {
                        for (int cx = x; cx < x + module.size.x; cx++)
                        {
                            taken[cx, cy] = true;
                        }
                    }

                    return true;
                }
            }
        }

        return false;
    }

    private bool canPlace (Module module, int x, int y)
    {
        if (x < 0 || y < 0) return false;

        for (int cy = y; cy < y + module.size.y; cy++)
        {
            if (cy >= spaceY) return false;
            for (int cx = x; cx < x + module.size.x; cx++)
            {
                if (cx >= spaceX || taken[cx, cy]) return false;
            }
        }

        return true;
    }
}
