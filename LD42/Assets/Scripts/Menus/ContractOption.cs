﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContractOption : MonoBehaviour {

    public StationMenu stationMenu;
    public Text goodsNameText;
    public Text paymentAmountText;
    public Image asteroidLevelImage;
    public Image pirateLevelImage;
    public int maxLevel = 4;

    private Contract contract;

    private void Awake ()
    {
        if (asteroidLevelImage.material != null) asteroidLevelImage.material = new Material (asteroidLevelImage.material);
        if (pirateLevelImage.material != null) pirateLevelImage.material = new Material (pirateLevelImage.material);
    }

    private void OnDestroy ()
    {
        if (asteroidLevelImage.material != null) Destroy (asteroidLevelImage.material);
        if (pirateLevelImage.material != null) Destroy (pirateLevelImage.material);
    }

    public void SetContract (Contract contract)
    {
        this.contract = contract;

        goodsNameText.text = contract.goodsName;
        paymentAmountText.text = contract.payment + "M";

        asteroidLevelImage.material.SetFloat ("_Percent", (contract.asteroidLevel + 1.0f) / maxLevel);
        pirateLevelImage.material.SetFloat ("_Percent", (contract.pirateLevel + 1.0f) / maxLevel);
    }

    public void ButtonPressed ()
    {
        if (contract != null) stationMenu.SelectContract (contract);
    }
}
