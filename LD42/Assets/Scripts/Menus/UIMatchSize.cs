﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class UIMatchSize : MonoBehaviour
{
    public RectTransform copyFrom;

    private RectTransform rt;

    private void Awake ()
    {
        rt = this.GetComponent<RectTransform> ();
    }

    private void Update ()
    {
        if(rt == null) rt = this.GetComponent<RectTransform> ();
        if (copyFrom != null) rt.sizeDelta = copyFrom.sizeDelta;
    }
}
