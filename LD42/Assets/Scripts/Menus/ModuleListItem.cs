﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModuleListItem : MonoBehaviour
{
    public Button button;
    public Text text;

    private StationMenu menu;
    private ModuleType type;

    public void SetModule (StationMenu menu, ModuleType type, bool isSelected)
    {
        this.menu = menu;
        this.type = type;
        text.text = type.name;
        button.interactable = !isSelected;
    }

    public void Select ()
    {
        menu.SelectModuleType (type);
    }
}
