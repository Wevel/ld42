﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuDropDown : MonoBehaviour
{
    public Button fallInButton;
    public Button moveOutButton;

    public float startY;
    public float onScreenY;
    public float gravity = 10;
    public float moveOutSpeed = 10;
    public float bounceDampening = 0.2f;
    public int numBounces = 2;

    private void Awake ()
    {
        Vector2 position = transform.position;
        position.y = startY;
        transform.position = position;
    }

    public void DropIn ()
    {
        if (fallInButton != null)
        {
            fallInButton.gameObject.SetActive (false);
            fallInButton.interactable = false;
        }
        StopAllCoroutines ();
        StartCoroutine (dropIn ());
    }

    public void MoveOut ()
    {
        if (moveOutButton != null)
        {
            moveOutButton.gameObject.SetActive (false);
            moveOutButton.interactable = false;
        }
        StopAllCoroutines ();
        StartCoroutine (moveOut ());
    }

    private IEnumerator dropIn ()
    {
        Vector2 position = transform.position;

        float velocity = 0;

        while (position.y > onScreenY)
        {
            velocity += gravity;

            position.y -= Time.deltaTime * velocity;
            if (position.y < onScreenY) position.y = onScreenY;
            transform.position = position;
            yield return null;
        }

        for (int i = 0; i < numBounces; i++)
        {
            velocity = -velocity * bounceDampening;
            position.y -= Time.deltaTime * velocity;
            transform.position = position;
            yield return null;

            while (position.y < onScreenY)
            {
                velocity += gravity;

                position.y -= Time.deltaTime * velocity;
                if (position.y < onScreenY) position.y = onScreenY;
                transform.position = position;
                yield return null;
            }

            while (position.y > onScreenY)
            {
                velocity += gravity;

                position.y -= Time.deltaTime * velocity;
                if (position.y < onScreenY) position.y = onScreenY;
                transform.position = position;
                yield return null;
            }
        }        

        position.y = onScreenY;
        transform.position = position;
        if (moveOutButton != null)
        {
            moveOutButton.gameObject.SetActive (true);
            moveOutButton.interactable = true;
        }
    }

    private IEnumerator moveOut ()
    {
        Vector2 position = transform.position;

        while (position.y < startY)
        {
            position.y += Time.deltaTime * moveOutSpeed;
            transform.position = position;
            yield return null;
        }

        position.y = startY;
        transform.position = position;
        if (fallInButton != null)
        {
            fallInButton.gameObject.SetActive (true);
            fallInButton.interactable = true;
        }
    }
}
