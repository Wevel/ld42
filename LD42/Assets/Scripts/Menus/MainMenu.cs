﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    public Game game;
    public MenuDropDown dropDown;
    public GameObject playButton;
    public GameObject restartButton;
    public GameObject resumeButton;
    public GameObject uiBlocker;

    public bool showingMenu = false;

    public void Start ()
    {
        dropDown.DropIn ();
        game.Pause ();
        showingMenu = true;
    }

    private void Update ()
    {
        if (Input.GetKeyDown (KeyCode.Escape))
        {
            if (game.paused)
            {
                dropDown.MoveOut ();
                game.UnPause ();
                showingMenu = false;
            }
            else
            {
                game.Pause ();
                dropDown.DropIn ();
                showingMenu = true;
            }
        }

        playButton.SetActive (game.player == null);
        restartButton.SetActive (game.player != null);
        resumeButton.SetActive (game.player != null);
        uiBlocker.SetActive (showingMenu);
    }

    public void StartButtonPressed ()
    {
        game.StartGame ();
        dropDown.MoveOut ();
        showingMenu = false;
    }

    public void ResumeButtonPressed ()
    {
        game.UnPause ();
        dropDown.MoveOut ();
        showingMenu = false;
    }

    public void ExitButtonPressed ()
    {
        Application.Quit ();
    }
}
