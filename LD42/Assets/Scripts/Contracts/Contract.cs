﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Contract
{
    public string goodsName;
    public uint payment;
    public uint bounty;
    public Station target;
    public int asteroidLevel;
    public int pirateLevel;

    public void AddBounty (uint value)
    {
        bounty += value;
    }
}
