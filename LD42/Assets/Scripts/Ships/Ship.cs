﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Ship
{
    private const float drag = 0.1f;

    public readonly Game game;

    public Color gizmosColor = Color.magenta;
    public float maxAcceleration = 2;
    public float maxVelocity = 1;
    public float maxAngleDelta = 200;
    public float maxHealth = 100;
    public float evadeChance = 0.1f;

    public Vector2 velocity { get; private set; }
    public Vector2 position { get; private set; }
    public float direction { get; private set; }
    public float targetDirection { get; protected set; }
    public float throttle { get; protected set; }
    public float health { get; protected set; }

    private List<Weapon> weapons = new List<Weapon> ();

    public Quaternion Rotation
    {
        get
        {
            return Quaternion.Euler (0, 0, direction);
        }
    }

    public float HealthPercent
    {
        get
        {
            return health / maxHealth;
        }
    }

    public void FullHeal ()
    {
        health = maxHealth;
    }

    public Ship (Game game, Vector2 position, float direction)
    {
        this.game = game;
        this.position = position;
        this.direction = direction;
        FullHeal ();

        game.SpawnShip (this);
    }

    public void Update (float deltaTime)
    {
        if (game.player != null && game.player.contract != null)
        {
            UpdateControl (deltaTime);

            direction = Mathf.MoveTowardsAngle (direction, targetDirection, maxAngleDelta * deltaTime);
            while (direction > 360) direction -= 360;
            while (direction < -360) direction += 360;

            Vector2 steeringForce = Rotation * Vector2.up * Mathf.Clamp01 (throttle) * maxAcceleration;

            Vector2 acceleration = Vector2.ClampMagnitude (steeringForce, maxAcceleration);
            velocity += acceleration * deltaTime;
            velocity = Vector2.ClampMagnitude (velocity, maxVelocity);

            if (velocity.sqrMagnitude > 0.002)
            {
                velocity -= velocity.normalized * velocity.sqrMagnitude * drag;
                position += velocity;
            }

            OnUpdate (deltaTime);

            for (int i = 0; i < weapons.Count; i++) weapons[i].Update (deltaTime);
        }
        else
        {
            velocity = Vector2.zero;
        }
    }

    public bool DoDamage (float damage, Vector2 direction)
    {
        //damage = TryShieldBlock (damage, direction);

        if (damage > 0)
        {
            health -= damage;
            if (health < 0)
            {
                health = 0;
                OnDeath ();
                game.ShipDied (this);
            }

            return true;
        }

        return false;
    }

    public void AddWeapon (Weapon weapon)
    {
        if (!weapons.Contains (weapon)) weapons.Add (weapon);
    }

    public void RemoveWeapon (Weapon weapon)
    {
        if (weapons.Contains (weapon)) weapons.Remove (weapon);
    }

    public virtual void OnDrawGizmos ()
    {
        Gizmos.color = gizmosColor;

        if (Application.isEditor && Input.GetKey (KeyCode.Z)) Gizmos.DrawSphere (position, 160);
        else Gizmos.DrawSphere (position, 16);


        Gizmos.DrawLine (position, (Vector3)position + (Rotation * new Vector2 (0, 8)));

        for (int i = 0; i < weapons.Count; i++)
        {
            Gizmos.DrawWireSphere (position, weapons[i].range);
        }
    }

    public abstract Ship GetClosestTargetInRange (float range);
    protected abstract void OnDeath ();
    protected abstract void UpdateControl (float deltaTime);

    protected virtual float TryShieldBlock ( float damage, Vector2 direction) { return damage; }
    protected virtual void OnUpdate (float deltaTime) { }

}
