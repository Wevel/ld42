﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : Ship
{
    private Station target;

    public NPC (Game game, Vector2 position, float direction) : base (game, position, direction)    
    {
        gizmosColor = Color.magenta;
        maxAcceleration = 4;
        maxVelocity = 0.3f;
        maxAngleDelta = 200;
        maxHealth = 100;
        evadeChance = 2f;
    }

    protected override void UpdateControl (float deltaTime)
    {
        throttle = 1.0f;

        if (target == null || Vector2.Distance(target.position, position) < 20)
        {
            target = game.GetNPCTarget (this);
        }

        Vector2 dir = target.position - position;
        targetDirection = Mathf.LerpAngle (targetDirection, Mathf.Atan2 (-dir.x, dir.y) * Mathf.Rad2Deg, deltaTime * maxAngleDelta * 0.5f);
    }

    protected override void OnDeath () { }

    public override Ship GetClosestTargetInRange (float range) { return null; }
}
