﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidChunk
{
    private static readonly Rect spawnRect = new Rect (-100, -100, 200, 200);

    public readonly int x;
    public readonly int y;
    public readonly Game game;
    public readonly Rect area;
    public List<Asteroid> asteroids = new List<Asteroid> ();

    public AsteroidChunk (int x, int y, Game game, Rect area, Vector2 perlinOffset, float perlinScale)
    {
        this.x = x;
        this.y = y;
        this.game = game;
        this.area = area;

        spawnAsteroids (perlinOffset, perlinScale);
    }

    public void Update (float deltaTime)
    {
        for (int i = 0; i < asteroids.Count; i++) asteroids[i].Update (deltaTime);
    }

    public bool Intersects (Rect r)
    {
        return area.Overlaps (r);
    }

    private void spawnAsteroids (Vector2 perlinOffset, float perlinScale)
    {
        const float minSeperation = 200;
        const float maxSeperation = 500;
        const int maxSpawnAttempts = 10;

        List<Asteroid> canSpawnFrom = new List<Asteroid> ();

        Asteroid a = new Asteroid (game, this, area.center + Random.insideUnitCircle * minSeperation);
        canSpawnFrom.Add (a);

        int index;

        while (canSpawnFrom.Count > 0)
        {
            index = Random.Range (0, canSpawnFrom.Count);
            a = addAsteroid (canSpawnFrom[index], minSeperation, maxSeperation, maxSpawnAttempts, perlinOffset, perlinScale);
            if (a == null) canSpawnFrom.RemoveAt (index);
            else canSpawnFrom.Add (a);
        }
    }

    private Asteroid addAsteroid (Asteroid from, float minRange, float maxRange, int attempts, Vector2 perlinOffset, float perlinScale)
    {
        float p = Mathf.PerlinNoise (perlinOffset.x + (from.position.x / perlinScale), perlinOffset.y + (from.position.y / perlinScale));

        float mod = 2.0f - Mathf.Clamp01 (from.position.magnitude / 2000);

        float min = Mathf.Lerp (minRange, minRange * 0.1f, p) * mod;
        float max = Mathf.Lerp (maxRange, maxRange * 0.1f, p) * mod;


        Vector2 test;
        for (int i = 0; i < attempts; i++)
        {
            test = from.position + Random.insideUnitCircle.normalized * Random.Range (min + 1, max);
            if (canSpawnAsteroid (test, minRange, perlinOffset, perlinScale))
            {
                Asteroid s = new Asteroid (game, this, test);
                asteroids.Add (s);
                return s;
            }
        }

        return null;
    }

    private bool canSpawnAsteroid (Vector2 point, float minRange, Vector2 perlinOffset, float perlinScale)
    {
        if (!area.Contains (point)) return false;
        if (spawnRect.Contains (point)) return false;
        if (!game.CanSpawnAsteroid (point, minRange)) return false;

        float mod = 2.0f - Mathf.Clamp01 (point.magnitude / 2000);
        float p = Mathf.PerlinNoise (perlinOffset.x + (point.x / perlinScale), perlinOffset.y + (point.y / perlinScale));
        float min = Mathf.Lerp (minRange * 0.1f, minRange, p) * mod;
        float r2 = min * min;
        return asteroids.Find (x => (point - x.position).sqrMagnitude < r2) == null;
    }
}
