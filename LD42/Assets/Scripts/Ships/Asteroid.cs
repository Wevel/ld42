﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid
{
    public readonly Game game;
    public readonly Vector2 position;
    public readonly AsteroidChunk chunk;

    public uint bounty = 10;
    public float collideDistance = 25;
    public float collideDamage = 25f;
    private float health = 25;

    public Asteroid (Game game, AsteroidChunk chunk, Vector2 position)
    {
        this.game = game;
        this.chunk = chunk;
        this.position = position;
    }

    public void Update (float deltaTime)
    {
        Vector2 delta = game.player.position - position;
        if ((position - game.player.position).sqrMagnitude < collideDistance * collideDistance)
        {
            game.player.DoDamage (collideDamage, game.player.position - position);
            ImpactShake.DoImpact (delta, 4);
            if (chunk.asteroids.Contains (this)) chunk.asteroids.Remove (this);
        }
    }

    public virtual void OnDrawGizmos ()
    {
        Gizmos.color = Color.yellow;
        if (Application.isEditor && Input.GetKey (KeyCode.Z)) Gizmos.DrawSphere (position, 160);
        else Gizmos.DrawSphere (position, 16);
    }

    public void DoDamage (float amount)
    {
        health -= amount;
        if (health < 0)
        {
            health = 0;
            game.player.contract.AddBounty (bounty);
            if (chunk.asteroids.Contains (this)) chunk.asteroids.Remove (this);
        }
    }
}
