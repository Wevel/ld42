﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LaserGlow : MonoBehaviour
{
    public static void ShowGlow ()
    {
        instance.showGlow = 5;
    }

    private static LaserGlow instance;

    public Image glowImage;
    public float glowAlpha = 0.2f;
    public float increaceRate = 0.2f;
    public float decreaceRate = 1.0f;
    private int showGlow;

    private void OnEnable ()
    {
        instance = this;
    }

    private void OnDisable ()
    {
        instance = this;
    }

    private void LateUpdate ()
    {
        if (showGlow > 0)
        {
            showGlow--;
            Color c = glowImage.color;
            if (c.a < glowAlpha) c.a = Mathf.Lerp (c.a, glowAlpha, increaceRate * Time.deltaTime);
            else c.a = glowAlpha;
            glowImage.color = c;
        }
        else
        {
            Color c = glowImage.color;
            if (c.a > 0) c.a = Mathf.Lerp (c.a, 0, decreaceRate * Time.deltaTime);
            else c.a = 0;
            glowImage.color = c;
        }
    }
}
