﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KineticTurret : Weapon
{
    public KineticTurret (Ship ship) : base (ship) { }

    protected override void UpdateTarget (float deltaTime)
    {
        if (target == null) target = ship.GetClosestTargetInRange (range);
    }

    protected override void DoDamageEffect (Vector2 direction)
    {
        ImpactShake.DoImpact (direction);

    }
}
