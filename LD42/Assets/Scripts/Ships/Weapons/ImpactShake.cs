﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImpactShake : MonoBehaviour
{
    public static void DoImpact (Vector2 direction)
    {
        Sound.PlayClip ("Impact");
        instance.doImpact (direction, 1f);
    }

    public static void DoImpact (Vector2 direction, float strength)
    {
        Sound.PlayClip ("Impact");
        instance.doImpact (direction, strength);
    }

    private static ImpactShake instance;

    public float duration = 0.2f;
    public float amplitude = 0.7f;
    public float cameraReturnSpeed = 10;

    Vector3 initialCameraLocation;

    private void OnEnable ()
    {
        instance = this;
        initialCameraLocation = transform.position;
    }

    private void OnDisable ()
    {
        instance = this;
    }

    private void doImpact (Vector2 direction, float strength)
    {
        StopAllCoroutines ();
        StartCoroutine (cameraShake (direction, strength));
    }

    private IEnumerator cameraShake (Vector3 direction, float strength)
    {
        direction.Normalize ();

        transform.position = initialCameraLocation + (direction * amplitude * strength);
        yield return null;
        transform.position = initialCameraLocation + (direction * amplitude * 0.9f) + (Random.insideUnitSphere * amplitude * strength * 0.1f);
        yield return null;
        transform.position = initialCameraLocation + (direction * amplitude * 0.5f) + (Random.insideUnitSphere * amplitude * strength * 0.5f);
        yield return null;

        float t = 0;

        while (t < duration)
        {
            t += Time.deltaTime;
            transform.position = initialCameraLocation + (Random.insideUnitSphere * amplitude * strength);

            yield return null;
        }

        while ((transform.position - initialCameraLocation).sqrMagnitude > Time.deltaTime * cameraReturnSpeed)
        {
            transform.position = Vector3.Lerp (transform.position, initialCameraLocation, Time.deltaTime * cameraReturnSpeed);
            yield return null;
        }

        transform.position = initialCameraLocation;
    }
}
