﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon
{
    public Ship ship { get; private set; }

    public float fireDelay;
    public float hitChance = 0.5f;
    public float damage = 5;
    public float range = 100;

    private float fireTimer;
    public Ship target { get; protected set; }

    public Weapon (Ship ship)
    {
        this.ship = ship;
    }

    public void Update (float deltaTime)
    {
        // Make sure to stop targeting if they go out of range
        if (target != null && ((target.position - ship.position)).sqrMagnitude > range * range) target = null;

        UpdateTarget (deltaTime);
        if (target != null)
        {
            if (fireDelay > 0)
            {
                if (fireTimer <= fireDelay)
                {
                    fireTimer += deltaTime;
                }
                else
                {
                    while (fireTimer > fireDelay)
                    {
                        fireTimer -= fireDelay;
                        TryDamage (ship, target, deltaTime);
                    }
                }
            }
            else
            {
                TryDamage (ship, target, deltaTime);
            }
        }
    }

    private void TryDamage (Ship source, Ship target, float deltaTime)
    {
        if (Random.value < hitChance)
        {
            if (Random.value < target.evadeChance) return;

            Vector2 direction = target.position - source.position;

            bool didDamage;
            if (fireDelay > 0) didDamage = target.DoDamage (damage, direction);
            else didDamage = target.DoDamage (damage * deltaTime, direction);

            if (didDamage)
            {
                if (target.GetType () == typeof (Player)) DoDamageEffect (direction);
            }
        }
    }

    protected abstract void UpdateTarget (float deltaTime);
    protected abstract void DoDamageEffect (Vector2 direction);
}
