﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserTurret : Weapon
{
    public LaserTurret (Ship ship) : base (ship)
    {
        hitChance = 0.9f;
    }

    protected override void UpdateTarget (float deltaTime)
    {
        if (target == null) target = ship.GetClosestTargetInRange (range);
    }

    protected override void DoDamageEffect (Vector2 direction)
    {
        LaserGlow.ShowGlow ();
        Sound.PlayClip ("Laser");
    }
}
