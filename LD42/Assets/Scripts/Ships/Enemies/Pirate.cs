﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pirate : Ship
{
    public uint bounty = 50;

    public Pirate (Game game, Vector2 position, float direction) : base (game, position, direction)
    {
        gizmosColor = Color.red;
        maxAcceleration = 2;
        maxVelocity = 0.8f;
        maxAngleDelta = 75;
        maxHealth = 75;
        evadeChance = 0.1f;
    }

    protected override void UpdateControl (float deltaTime)
    {
        float dist = Vector2.Distance (position, game.player.position);

        Vector2 target = Vector2.zero;
        if (dist > 300) target = Seek (game.player.position - (Vector2)(game.player.Rotation * Vector2.up * 25));
        else if (dist < 50) target = Evade () * 4f;
        else target = Pursuit ();

        throttle = target.magnitude;
        targetDirection = Mathf.LerpAngle (targetDirection, Mathf.Atan2 (-target.x, target.y) * Mathf.Rad2Deg, deltaTime * maxAngleDelta * 0.25f);
    }

    protected override void OnDeath ()
    {
        game.player.contract.AddBounty (bounty);
        game.enemySpawner.Despawn (this);
    }

    public override Ship GetClosestTargetInRange (float range)
    {
        if (((Vector2)(game.player.position - position)).sqrMagnitude < range * range) return game.player;
        else return null;
    }

    private Vector2 Seek (Vector2 target)
    {
        Vector2 targetVelocity = (target - position).normalized * maxVelocity;
        return targetVelocity - velocity;
    }

    private Vector2 Arrive (Vector2 target, float deceleration)
    {
        Vector2 toTarget = target - position;
        float distance = toTarget.magnitude;

        if (distance > 0)
        {
            float speed = distance / deceleration;
            speed = Mathf.Min (speed, maxVelocity);

            Vector2 targetVelocity = toTarget * speed / distance;
            return targetVelocity - velocity;
        }
        else
        {
            return Vector2.zero;
        }
    }

    private Vector2 Pursuit ()
    {
        Vector2 toTarget = game.player.position - (Vector2)(game.player.Rotation * Vector2.up * 25) - position;
        float relativeHeading = Vector2.Dot (Rotation * Vector2.up, game.player.Rotation * Vector2.up);

        if (Vector2.Dot (toTarget, Rotation * Vector2.up) > 0 && relativeHeading < 0.95f)
            return Seek (game.player.position);

        float lookAheadTime = toTarget.magnitude / (maxVelocity + game.player.maxVelocity);
        return Seek ((Vector2)game.player.position + (game.player.velocity * lookAheadTime));
    }

    private Vector2 Evade ()
    {
        Vector2 toTarget = game.player.position - position;

        float lookAheadTime = toTarget.magnitude / (maxVelocity + game.player.maxVelocity);
        return Seek ((Vector2)game.player.position + (game.player.velocity * lookAheadTime));
    }
}
