﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public Pirate piratePrefab;
    public float spawnRange = 500;

    private List<Pirate> enemies = new List<Pirate> ();

    private Game game;

    private int enemiesToSpawn = 0;
    private bool spawnEnemies = false;
    private float spawnDistances;

    public bool havePiratesSpawned { get; private set; }

    public IEnumerable<Pirate> Enemies
    {
        get
        {
            for (int i = 0; i < enemies.Count; i++) yield return enemies[i];
        }
    }
    
    private void Awake ()
    {
        game = GetComponent<Game> ();
    }

    private void Update ()
    {
        if (spawnEnemies)
        {
            if (enemiesToSpawn > 0)
            {
                if (Vector2.Distance (game.player.position, game.player.contract.target.position) < spawnDistances * (enemiesToSpawn + 1))
                {
                    Spawn ();
                    enemiesToSpawn--;
                }
            }
            else
            {
                spawnEnemies = false;
                enemiesToSpawn = 0;
            }
        }
    }

    public void Spawn ()
    {
        havePiratesSpawned = true;
        Vector2 spawnPosition = game.player.position + (Random.insideUnitCircle.normalized * spawnRange);

        Pirate ship = new Pirate (game, spawnPosition, Quaternion.FromToRotation (Vector2.up, (Vector2)game.player.position - spawnPosition).eulerAngles.z);

        if (Random.value < 0.35f) ship.AddWeapon (new LaserTurret (ship));
        else ship.AddWeapon (new KineticTurret (ship));
        enemies.Add (ship);
    }

    public void ContractStarted ()
    {
        if (game.player.contract.pirateLevel > 0)
        {
            enemiesToSpawn = game.player.contract.pirateLevel;
            spawnEnemies = true;
            spawnDistances = Vector2.Distance (game.player.position, game.player.contract.target.position) / (game.player.contract.pirateLevel + 2);
        }
        else
        {
            spawnEnemies = false;
            enemiesToSpawn = 0;
        }
    }

    public void Despawn (Pirate ship)
    {
        if (enemies.Contains (ship)) enemies.Remove (ship);
    }

    public void DespawnAll ()
    {
        for (int i = 0; i < enemies.Count; i++) game.ShipDied (enemies[i]);
        enemies.Clear ();
        spawnEnemies = false;
        enemiesToSpawn = 0;
        havePiratesSpawned = false;
    }
}
