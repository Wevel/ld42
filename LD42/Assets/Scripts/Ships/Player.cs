﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Ship
{
    public Contract contract { get; private set; }
    public bool turnRight;
    public bool turnLeft;
    public float offsetTurnRate = 160;
    private float turnRate;
    public float shieldRegenRate = 0.05f;
    public float shieldAbsorbAmount = 0.01f;
    public float reachTargetRange = 15;
    public float boostFuelUse = 0.5f;
    public float turnFuelUse = 0.5f;
    public float maxTurnRate = 90;
    public float minTurnRate = 20;

    public float maxFuel = 70;
    public float fuel { get; private set; }
    public float FuelPercent
    {
        get
        {
            return fuel / maxFuel;
        }
    }

    private float[] engineLevels = new float[2];
    private float[] shieldLevels = new float[4];

    public int shieldBoostIndex = -1;

    private List<Station> visitedStations = new List<Station> ();

    private List<Armour> armourModules = new List<Armour> ();
    private List<FuelTank> fuelModules = new List<FuelTank> ();

    public Player (Game game, Vector2 position, float direction) : base (game, position, direction)
    {
        gizmosColor = Color.white;
        maxAcceleration = 4;
        maxVelocity = 0.3f;
        maxAngleDelta = 200;
        maxHealth = 100;
        evadeChance = 0.1f;
    }

    public bool BeenToStation (Station s)
    {
        return visitedStations.Contains (s);
    }

    public void VisitStation (Station s)
    {
        if (!visitedStations.Contains (s)) visitedStations.Add (s);
    }

    public void FullRefuel ()
    {
        fuel = maxFuel;
    }

    public void StartContract (Contract contract)
    {
        
        this.contract = contract;

        for (int i = 0; i < engineLevels.Length; i++) engineLevels[i] = 0;
        for (int i = 0; i < shieldLevels.Length; i++) shieldLevels[i] = 0;
    }

    public void BoostEngine (int index)
    {
        if (fuel > 0)
        {
            Sound.PlayClip ("Boost");
            engineLevels[index] = Mathf.Clamp01 (engineLevels[index] + Random.Range (0.12f, 0.18f));
            fuel -= boostFuelUse;
        }
    }

    public float GetShieldLevel (int index)
    {
        return shieldLevels[index];
    }

    public float GetEngineLevel (int index)
    {
        return engineLevels[index];
    }

    protected override void UpdateControl (float deltaTime)
    {
        float[] engineThrust = new float[2];

        // Figue out thrust from each engine
        for (int i = 0; i < engineLevels.Length; i++)
        {
            if (engineLevels[i] > 0.75f) engineThrust[i] = 0.35f;
            else if (engineLevels[i] > 0.5f) engineThrust[i] = 0.25f;
            else if (engineLevels[i] > 0.25f) engineThrust[i] = 0.15f;
            else if (engineLevels[i] > 0.05f) engineThrust[i] = 0.025f;
            else engineThrust[i] = 0;
        }

        // Calculate twist from unbalanced side engines
        // If they are the same, the first part is 0
        targetDirection += (engineThrust[1] - engineThrust[0]) * offsetTurnRate* deltaTime * 0.25f;

        // Actually set the thrust
        throttle = engineThrust[0] + engineThrust[1];

        if (fuel > 0)
        {
            if (turnRight || turnLeft)
            {
                turnRate += deltaTime * 50;
                turnRate = Mathf.Clamp (turnRate, minTurnRate, maxTurnRate);
                fuel -= turnFuelUse * deltaTime * turnRate / maxTurnRate;
            }
        }
        else
        {
            turnRate = Mathf.Lerp (turnRate, 0, deltaTime * 50);
        }

        if (turnRight) targetDirection -= deltaTime * turnRate;
        else if (turnLeft) targetDirection += deltaTime * turnRate;
        else
        {
            turnRate -= deltaTime * 50;
            turnRate = Mathf.Clamp (turnRate, minTurnRate, maxTurnRate);
        }
    }

    public override Ship GetClosestTargetInRange (float range)
    {
        Pirate best = null;
        float bestDist = float.PositiveInfinity;
        float tmpDist;

        foreach (Pirate item in game.enemySpawner.Enemies)
        {
            tmpDist = ((item.position - position)).sqrMagnitude;
            if (tmpDist < range * range && (tmpDist < bestDist || best == null))
            {
                best = item;
                bestDist = tmpDist;
            }
        }

        return best;
    }

    public override void OnDrawGizmos ()
    {
        base.OnDrawGizmos ();
        if (contract != null) Gizmos.DrawLine (position, contract.target.position);
    }

    protected override void OnUpdate (float deltaTime)
    {
        List<Armour> newArmour = game.modules.FindAllModules<Armour> ();
        List<Armour> currentArmour = new List<Armour> (armourModules);
        for (int i = 0; i < newArmour.Count; i++)
        {
            if (newArmour[i].state == ModuleState.Running)
            {
                if (currentArmour.Contains (newArmour[i])) currentArmour.Remove (newArmour[i]);
                else
                {
                    armourModules.Add (newArmour[i]);
                    maxHealth += newArmour[i].healthIncreace;
                    health += newArmour[i].healthIncreace;
                }
            }
        }

        for (int i = 0; i < currentArmour.Count; i++)
        {
            armourModules.Remove (currentArmour[i]);
            maxHealth -= currentArmour[i].healthIncreace;
            health -= currentArmour[i].healthIncreace;
        }

        List<FuelTank> newFuelTanks = game.modules.FindAllModules<FuelTank> ();
        List<FuelTank> currentFuelTanks = new List<FuelTank> (fuelModules);
        for (int i = 0; i < newFuelTanks.Count; i++)
        {
            if (newFuelTanks[i].state == ModuleState.Running)
            {
                if (currentFuelTanks.Contains (newFuelTanks[i])) currentFuelTanks.Remove (newFuelTanks[i]);
                else
                {
                    fuelModules.Add (newFuelTanks[i]);
                    maxFuel += newFuelTanks[i].fuelIncreace;
                }
            }
        }

        for (int i = 0; i < currentFuelTanks.Count; i++)
        {
            fuelModules.Remove (currentFuelTanks[i]);
            maxFuel -= currentFuelTanks[i].fuelIncreace;
        }

        bool allZero = true;

        for (int i = 0; i < engineLevels.Length; i++)
        {
            if (engineLevels[i] > 0)
            {
                engineLevels[i] -= deltaTime * 0.05f;
                allZero = false;
            }
            else engineLevels[i] = 0;
        }

        if (shieldBoostIndex >= 0 && shieldBoostIndex < shieldLevels.Length)
        {
            shieldLevels[shieldBoostIndex] += deltaTime * shieldRegenRate;
            shieldLevels[shieldBoostIndex] = Mathf.Clamp01 (shieldLevels[shieldBoostIndex]);
        }

        if ((position - contract.target.position).sqrMagnitude < reachTargetRange * reachTargetRange)
        {
            game.CompleteCurrentContract ();
        }

        if (!(health > 0))
        {
            game.ShipDied (this);
            OnDeath ();
        }

        if (!(fuel > 0))
        {
            if (allZero)
            {
                game.ShipDied (this);
                OnDeath ();
            }
        }
    }

    protected override float TryShieldBlock (float damage, Vector2 dir)
    {
        dir = Quaternion.Euler (0, 0, -direction) * dir;

        int shieldIndex = -1;
        if (dir.x > 0)
        {
            if (dir.y > 0) shieldIndex = 0;
            else shieldIndex = 1;
        }
        else
        {
            if (dir.y > 0) shieldIndex = 3;
            else shieldIndex = 2;
        }

        if (shieldIndex >= 0 && shieldLevels[shieldIndex] > 0)
        {
            float blockable = damage * shieldAbsorbAmount;
            if (blockable >= shieldLevels[shieldIndex])
            {
                shieldLevels[shieldIndex] -= blockable;
                return 0;
            }
            else
            {
                blockable = shieldLevels[shieldIndex];
                shieldLevels[shieldIndex] = 0;

                return damage - (blockable / shieldAbsorbAmount);
            }
        }

        return damage;
    }

    protected override void OnDeath () { }
}
