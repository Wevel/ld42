﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Station : Ship
{
    public float dist;
    public float visitDistance = 100;

    public Station (Game game, Vector2 position, float direction) : base (game, position, direction)
    {
        gizmosColor = Color.blue;
        maxAcceleration = 0;
        maxVelocity = 0;
        maxAngleDelta = 0;
        maxHealth = float.PositiveInfinity;
        evadeChance = 0f;
    }

    protected override void UpdateControl (float deltaTime) { }
    protected override void OnDeath () { }
    public override Ship GetClosestTargetInRange (float range) { return null; }

    protected override void OnUpdate (float deltaTime)
    {
        if ((position - game.player.position).sqrMagnitude < visitDistance * visitDistance) game.player.VisitStation (this);
    }
}
