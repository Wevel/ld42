﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialItem
{
    public readonly string name;
    public readonly bool canRepeat;
    public readonly bool overrideable;
    public readonly bool runIfNoCurrent;
    private readonly IsTriggered isTriggered;
    private readonly RunTutorial run;

    private Coroutine controlCoroutine;
    private Coroutine coroutine;

    public TutorialItem (string name, bool canRepeat, bool overrideable, bool runIfNoCurrent, IsTriggered isTriggered, RunTutorial run)
    {
        this.name = name;
        this.canRepeat = canRepeat;
        this.overrideable = overrideable;
        this.runIfNoCurrent = runIfNoCurrent;
        this.isTriggered = isTriggered;
        this.run = run;
    }

    public void Stop (Tutorial tutorial)
    {
        if (controlCoroutine != null) tutorial.StopCoroutine (controlCoroutine);
        if (coroutine != null) tutorial.StopCoroutine (coroutine);
        controlCoroutine = null;
        coroutine = null;
    }

    public void Start (Tutorial tutorial)
    {
        Debug.Log ("Starting tutorial - " + name);
        if (controlCoroutine == null) controlCoroutine = tutorial.StartCoroutine (controlledRun (tutorial));
    }

    public bool ShouldStart ()
    {
        return isTriggered ();
    }

    private IEnumerator controlledRun (Tutorial tutorial)
    {
        coroutine = tutorial.StartCoroutine (run ()); ;
        yield return coroutine;
        controlCoroutine = null;
        coroutine = null;
    }
}
