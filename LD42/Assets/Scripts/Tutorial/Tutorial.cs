﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public delegate bool IsTriggered ();
public delegate IEnumerator RunTutorial ();

public class Tutorial : MonoBehaviour {

    public Game game;
    public Text outputText;
    public GameObject uiBlocker;
    public float moveSpeed = 200;
    public Vector2 offSceenPosition;
    public Vector2 onScreenPosition;
    public GameObject clickToContinue;
    public MainMenu mainMenu;
    public GameObject skipButton;

    private TutorialItem currentItem;

    private Coroutine moveCoroutine;

    private List<TutorialItem> tutorialItems = new List<TutorialItem> ();
    private Queue<TutorialItem> toRun = new Queue<TutorialItem> ();
    private bool started = false;
    //private float startTime;

    public bool IsMoving
    {
        get
        {
            return moveCoroutine != null;
        }
    }

    private void Awake ()
    {
        unblockUI ();
    }

    public void StartTutorial ()
    {
        StopAllCoroutines ();
        moveOffScreen ();
        Skip ();
        //startTime = Time.time;
        tutorialItems.Clear ();
        toRun.Clear ();
        setupTutorialItems ();
        started = true;
    }
	
	void Update ()
    {
        skipButton.SetActive (game.deathType == Game.DeathType.NotDead);

        if (started)
        {
            for (int i = tutorialItems.Count - 1; i >= 0; i--)
            {
                if (tutorialItems[i].ShouldStart ())
                {
                    if (!tutorialItems[i].runIfNoCurrent || currentItem == null)
                    {
                        toRun.Enqueue (tutorialItems[i]);
                        tutorialItems.RemoveAt (i);
                    }
                }
            }

            if (toRun.Count > 0)
            {
                if (currentItem != null && currentItem.overrideable) Skip ();
                if (currentItem == null)
                {
                    currentItem = toRun.Dequeue ();
                    if (!(currentItem.runIfNoCurrent && toRun.Count > 0))
                    {
                        currentItem.Start (this);
                    }
                }
            }
        }
	}

    public void Skip ()
    {
        if (currentItem != null)
        {
            currentItem.Stop (this);
            StopAllCoroutines ();
            finishCurrentItem ();
        }
    }

    private void finishCurrentItem ()
    {
        unblockUI ();
        if (currentItem != null)
        {
            if (currentItem.canRepeat) tutorialItems.Add (currentItem);
            moveOffScreen ();
            currentItem = null;
        }
    }

    private void moveOnScreen ()
    {
        if (moveCoroutine != null) StopCoroutine (moveCoroutine);
        moveCoroutine = StartCoroutine (move (onScreenPosition));
    }

    private void moveOffScreen ()
    {
        if (moveCoroutine != null) StopCoroutine (moveCoroutine);
        moveCoroutine = StartCoroutine (move (offSceenPosition));
    }

    private IEnumerator move (Vector2 target)
    {
        RectTransform rt = GetComponent<RectTransform> ();

        float dist;
        while ((dist = Vector2.Distance (target, rt.anchoredPosition)) > Time.deltaTime * moveSpeed)
        {
            rt.anchoredPosition = Vector2.Lerp (rt.anchoredPosition, target, Time.deltaTime * moveSpeed / dist);
            yield return null;
        }

        rt.anchoredPosition = target;

        moveCoroutine = null;
    }

    private void blockUI ()
    {
        uiBlocker.SetActive (true);
    }

    private void unblockUI ()
    {
        uiBlocker.SetActive (false);
    }

    private void addTutorialItem (string name, bool canRepeat, bool overrideable, bool runIfNoCurrent, RunTutorial run, IsTriggered isTriggered)
    {
        tutorialItems.Add (new TutorialItem (name, canRepeat, overrideable, runIfNoCurrent, isTriggered, run));
    }

    private void setHighlight (string name, bool value)
    {

    }

    private IEnumerator showText (string text, bool waitForClick = true)
    { 
        moveOffScreen ();
        while (IsMoving) yield return null;

        outputText.text = text;

        yield return new WaitForSeconds (0.2f);

        moveOnScreen ();
        while (IsMoving) yield return null;

        clickToContinue.SetActive (waitForClick);

        if (waitForClick)
        {
            while (!Input.GetMouseButtonUp (0))
            {
                while (mainMenu.showingMenu) yield return null;
                yield return null;
            }

        }
    }

    private void setupTutorialItems ()
    {
        addTutorialItem ("Distance", true, false, false, showDistance,
            () =>
            {
                return game.modules.FindModule<DistanceCounter> () != null && game.startedContract;
            });

        addTutorialItem ("Intro", false, false, false, runIntro,
            () =>
            {
                return true;
            });

        addTutorialItem ("FirstSpaceStation", false, false, false, runFirstShop,
            () =>
            {
                return game.stationMenu.inShop;
            });

        addTutorialItem ("GetShield", false, false, false, getShield,
            () =>
            {
                return game.modules.FindModule<Shield> () != null;
            });

        addTutorialItem ("GetShield", false, false, false, pirateSpawned,
           () =>
           {
               return game.enemySpawner.havePiratesSpawned;
           });

        addTutorialItem ("GetLasers", false, false, false, getLasers,
            () =>
            {
                return game.modules.FindModule<LaserTurretControl> () != null;
            });

        addTutorialItem ("GetKinetic", false, false, false, getKinetic,
          () =>
          {
              return game.modules.FindModule<KineticTurretControl> () != null;
          });

        addTutorialItem ("NoHealth", false, false, false, noHealth,
            () =>
            {
                return game.deathType == Game.DeathType.NoHealth;
            });

        addTutorialItem ("NoFuel", false, false, false, noFuel,
            () =>
            {
                return game.deathType == Game.DeathType.NoFuel;
            });
    }

    private IEnumerator noHealth ()
    {
        blockUI ();
        yield return StartCoroutine (showText ("Looks like your ship is falling apart, too much hitting asteroids and getting shot by pirates."));
        yield return StartCoroutine (showText ("At least you managed to eject in time."));

        if (game.completedContracts <= 2) yield return StartCoroutine (showText ("You only managed to do " + game.completedContracts + " contracts."));
        else if (game.completedContracts <= 10) yield return StartCoroutine (showText ("At least you managed to do " + game.completedContracts + " contracts."));
        else yield return StartCoroutine (showText ("Well done, you still managed to do " + game.completedContracts + " contracts."));

        mainMenu.Start ();
        unblockUI ();
        moveOffScreen ();

        yield return null;
        finishCurrentItem ();
    }

    private IEnumerator noFuel ()
    {
        blockUI ();
        yield return StartCoroutine (showText ("Oh no, look like your out of fuel, and there is a pirate ship heading in your direction."));
        yield return StartCoroutine (showText ("Looks like that ship isn't going to be yours for much longer."));

        if (game.completedContracts <= 2)  yield return StartCoroutine (showText ("You only managed to do " + game.completedContracts + " contracts."));
        else if (game.completedContracts <= 10) yield return StartCoroutine (showText ("At least you managed to do " + game.completedContracts + " contracts."));
        else yield return StartCoroutine (showText ("Well done, you still managed to do " + game.completedContracts + " contracts."));

        mainMenu.Start ();
        unblockUI ();
        moveOffScreen ();

        yield return null;
        finishCurrentItem ();
    }

    private IEnumerator runIntro ()
    {
        blockUI ();

        yield return StartCoroutine (showText ("Hello, it me, Gregory. I see you found the ship I got for you."));
        yield return StartCoroutine (showText ("The ship is a good size and can hold all the cargo you will ever need."));
        yield return StartCoroutine (showText ("However, it seems the old owner took all his control systems with him. I've found a few old ones that you can use for now."));
        yield return StartCoroutine (showText ("It seems that the radar is not installed, how about you have a go at doing that yourself."));
        yield return StartCoroutine (showText ("You can find the part in the parts locker above you, grab it and place it on the control panel in front of you.", false));
        setHighlight ("PartsMenu", true);

        Radar radar = game.modules.FindModule<Radar> ();
        if (radar == null)
        {
            Debug.LogError ("No radar unlocked");
            yield break;
        }

        unblockUI ();

        while (!radar.placed) yield return null;

        setHighlight ("PartsMenu", false);

        yield return StartCoroutine (showText ("Now, put in a screw in each corner to finish installing it.", false));

        while (radar.state != ModuleState.Starting && radar.state != ModuleState.Running) yield return null;

        yield return StartCoroutine (showText ("Right, your now all good to go, you ahead and turn on the navigation and engines.", false));

        setHighlight ("NavigationButton", true);
        setHighlight ("EnginesButton", true);

        Navigation nav = game.modules.FindModule<Navigation> ();
        if (nav == null)
        {
            Debug.LogError ("No navigation unlocked");
            yield break;
        }

        Engines engines = game.modules.FindModule<Engines> ();
        if (engines == null)
        {
            Debug.LogError ("No engines unlocked");
            yield break;
        }

        while ((nav.state != ModuleState.Starting && nav.state != ModuleState.Running) || (engines.state != ModuleState.Starting && engines.state != ModuleState.Running)) yield return null;

        blockUI ();

        setHighlight ("NavigationButton", false);
        setHighlight ("EnginesButton", false);

        yield return StartCoroutine (showText ("And finally you can head off. I've loaded up some cargo for you to take to a nearby space station"));
        yield return StartCoroutine (showText ("A sort of first contract, I'll pay you when you get there. Make sure you avoid the asteroids. Good luck."));

        moveOffScreen ();

        yield return new WaitForSeconds (2.5f);

        yield return StartCoroutine (showText ("Oh, I should also tell you how the navigation works. The green marker is your ships direction."));
        yield return StartCoroutine (showText ("While the blue marker is the velocity and the white marker is the direction of the target space station."));
        yield return StartCoroutine (showText ("Also, you can you the left and right arrows to steer, this does require fuel."));

        yield return StartCoroutine (showText ("For the engines, this ship has two. You can boost each one separately, doing so will consume some fuel."));
        yield return StartCoroutine (showText ("The higher the boost level, the faster you will go. ."));
        yield return StartCoroutine (showText ("Also try to keep them balanced so that you don’t go around in circles."));

        moveOffScreen ();
        unblockUI ();

        yield return null;
        finishCurrentItem ();
    }

    private IEnumerator runFirstShop ()
    {
        blockUI ();

        yield return StartCoroutine (showText ("Ah I see you got here and got your payment."));
        yield return StartCoroutine (showText ("Here is the space station market, you can buy new control modules."));
        yield return StartCoroutine (showText ("Also while your here, you should make sure you refuel and repair any damage to your ship."));
        yield return StartCoroutine (showText ("When you are done here, I'll go over finding new contracts.", false));
        unblockUI ();

        while (game.stationMenu.inShop) yield return null;

        blockUI ();
        yield return StartCoroutine (showText ("Here you can find a few contracts to select from for your next trip."));
        yield return StartCoroutine (showText ("Each one has what the goods are and how much you will get payed."));
        yield return StartCoroutine (showText ("It also has the estimated number of asteroids and pirates that you will find."));
        yield return StartCoroutine (showText ("It's probably best to avoid any pirates for now, but pick whatever contract you feel you can do."));
        yield return StartCoroutine (showText ("But do remember, your here to make money, so sometimes risks can be taken."));

        moveOffScreen ();
        unblockUI ();

        yield return null;
        finishCurrentItem ();
    }

    private static string[] distanceTexts = new string[] 
    {
        "Nice job getting that contract, you should need to travel a distance of ",
        "Another contract well done, this time you should need to travel a distance of ",
        "I see you are doing well with the contracts, by my maps you should need to travel a distance of "
    };

    private IEnumerator showDistance ()
    {
        game.startedContract = false;
        float distance = Vector2.Distance (game.player.position, game.player.contract.target.position) * 0.1f;
        int showDistance = Mathf.RoundToInt (distance / 10f) * 10;

        yield return StartCoroutine (showText (distanceTexts[Random.Range (0, distanceTexts.Length)] + showDistance + ".", false));
        yield return new WaitForSeconds (10);
        moveOffScreen ();

        yield return null;
        finishCurrentItem ();
    }

    private IEnumerator getShield ()
    {
        blockUI ();

        moveOffScreen ();
        unblockUI ();

        yield return null;
        finishCurrentItem ();
    }

    private IEnumerator pirateSpawned ()
    {
        yield return StartCoroutine (showText ("Looks like one of those filthy pirates has come after you, watch out for a moving dot on the radar.", false));

        yield return new WaitForSeconds (10);

        moveOffScreen ();

        yield return null;
        finishCurrentItem ();
    }

    private IEnumerator getLasers ()
    {
        blockUI ();
        yield return StartCoroutine (showText ("Pew pew, I see you got some lasers there."));
        yield return StartCoroutine (showText ("These can be used to shoot down those pirates, and you might as well shoot some asteroids as well."));
        yield return StartCoroutine (showText ("Doing this will allow you get a bounty for each pirate shop or asteroid destroyed."));
        yield return StartCoroutine (showText ("A bit of extra cash, isn't so bad."));
        yield return StartCoroutine (showText ("According to the instruction manual, you can just install the control system and let the turret charge up."));
        yield return StartCoroutine (showText ("Then, when there is target, you can just hit fire and the target will be blasted away."));
        yield return StartCoroutine (showText ("Or at least it will after a few shots, just make sure you are being careful when firing around a space station."));

        moveOffScreen ();
        unblockUI ();

        yield return null;
        finishCurrentItem ();
    }

    private IEnumerator getKinetic ()
    {
        blockUI ();
        yield return StartCoroutine (showText ("Ah, some good old projectile weapons, not that the air rifle there will do much."));
        yield return StartCoroutine (showText ("You can you them to shoot down pirates or asteroid, although they are set up to target the pirates first."));
        yield return StartCoroutine (showText ("To use them, just wait till there is a target and simply hold down that fire button."));

        moveOffScreen ();
        unblockUI ();

        yield return null;
        finishCurrentItem ();
    }
}
