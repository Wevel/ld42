﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SoundFX
{
    public string name;
    public AudioClip clip;
    public float modifierVolume = 1;
    public bool restartable = false;

    [System.NonSerialized]
    public bool playing = false;
    [System.NonSerialized]
    public AudioSource playingSource;
}
