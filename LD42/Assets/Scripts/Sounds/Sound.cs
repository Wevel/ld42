﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sound : MonoBehaviour
{
    public static void PlayClip (string name)
    {
        instance.playClip (name);
    }

    public static void StopClip (string name)
    {
        SoundFX fx = instance.getClip (name);
        if (fx != null && fx.playingSource != null) fx.playingSource.Stop ();
    }

    public static bool IsPlaying (string name)
    {
        SoundFX fx =instance.getClip (name);
        return fx != null && fx.playing;
    }

    public static float Volume
    {
        get
        {
            return instance.volume;
        }
        set
        {
            instance.volume = value;
        }
    }

    public AudioSource sourcePrefab;
    public List<SoundFX> clips = new List<SoundFX> ();

    private static Sound instance;

    private Queue<AudioSource> oldSources = new Queue<AudioSource> ();

    private float volume = 1.0f;

    private void OnEnable ()
    {
        instance = this;
        DontDestroyOnLoad (gameObject);
    }

    private void OnDisable ()
    {
        instance = null;
    }

    private void playClip (string name)
    {
        SoundFX clip = getClip (name);
        if (clip != null)
        {
            if (clip.restartable || !clip.playing) StartCoroutine (playSound (clip));
        }
    }

    private SoundFX getClip (string name)
    {
        return clips.Find (x => x.name == name);
    }

    private AudioSource getAudioSource ()
    {
        if (oldSources.Count > 0) return oldSources.Dequeue ();
        else return Instantiate (sourcePrefab, transform);
    }

    private IEnumerator playSound (SoundFX clip)
    {
        AudioSource source = getAudioSource ();
        source.gameObject.SetActive (true);

        clip.playingSource = source;
        clip.playing = true;
        source.clip = clip.clip;
        source.loop = false;
        source.volume = volume * clip.modifierVolume;
        source.Play ();

        while (source.isPlaying)
        {
            source.volume = volume * clip.modifierVolume;
            yield return null;
        }

        clip.playingSource = null;
        clip.playing = false;
        source.gameObject.SetActive (false);
        oldSources.Enqueue (source);
    }
}
